package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;

public class dbConnection
{	
	static DB cloudmanDB;
	static ArrayList<DBCollection> pmCollections = new ArrayList<DBCollection>();
	static ArrayList<DBCollection> vmCollections = new ArrayList<DBCollection>();	
	
	static ArrayList<String> pmIDs = new ArrayList<String>();
	static ArrayList<String> vmIDs = new ArrayList<String>();
	
	static ArrayList<String> pmIPs = new ArrayList<String>();
	static ArrayList<String> vmIPs = new ArrayList<String>();
	static ArrayList<String> allIPs = new ArrayList<String>();
	
	static HashMap<String,String> mapVMIDIP = new HashMap<String,String>();
	static HashMap<String,String> mapVMIDpmIP = new HashMap<String,String>();
	static HashMap<String,String> mapIDIP = new HashMap<String,String>();
	
	public dbConnection(String dbIP, int dbPort, String dbName) throws MongoException
	{
		try {		
			Mongo mongoClient = new Mongo(dbIP, dbPort);
			cloudmanDB = mongoClient.getDB(dbName);			
			setCollection(); 
		
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}	
	}
	
	public dbConnection(){}
	
	public static DB getCurrentDB()
	{
		return cloudmanDB;
	}
	
	public static void setCollection()
	{
		pmCollections.clear(); // used
		vmCollections.clear();
		pmIDs.clear(); 
		vmIDs.clear(); // used
		pmIPs.clear(); // used
		vmIPs.clear(); // used
		allIPs.clear(); // used
		
		Set<String> set = cloudmanDB.getCollectionNames();
		
		for(String string:set) // get names of PM/VM
		{
			DBCollection coll = cloudmanDB.getCollection(string);
			
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();		
			query.put("isPM",1);
			query.put("ip",1);
					
			Cursor cursor = coll.find(field,query);			
			BasicDBObject result = (BasicDBObject)cursor.next();

			String isPM = (String)result.get("isPM");
			String ip = (String)result.get("ip");	
			
			if(isPM == null)
				continue;
				
			else if(isPM.equals("1"))
			{
				pmCollections.add(cloudmanDB.getCollection(string));
				pmIDs.add(string);
				pmIPs.add(ip);
				allIPs.add(ip);
				mapIDIP.put(string, ip);
			}
			
			else
			{
				vmCollections.add(cloudmanDB.getCollection(string));
				vmIDs.add(string);
				vmIPs.add(ip);
				allIPs.add(ip);
				mapVMIDIP.put(string, ip);
				mapVMIDpmIP.put(string, string.split("/")[1]);
				mapIDIP.put(string, ip);
			}
		}	
	}
	
	public static int getNumCollection()
	{
		int numPMs = 0;
		
		numPMs = pmCollections.size();
		
		return numPMs;
	}
	
	public static ArrayList<DBCollection> getPMCollections()
	{
		return pmCollections;	
	}
	
	public static ArrayList<String> getPMIDs()
	{
		return pmIDs;	
	}
	
	public static ArrayList<String> getPMIPs()
	{
		return pmIPs;	
	}
	
	
	public static DBCollection getVMCollection(String vmID)
	{
		return cloudmanDB.getCollection(vmID);
	}
	
	public static DBCollection getCollection(String vmID)
	{
		return cloudmanDB.getCollection(vmID);
	}

//======================= map ==============================//	
	public static String getVMIPfromVMID(String vmID)
	{
		return mapVMIDIP.get(vmID);
	}

	public static String getPMIPfromVMID(String vmID)
	{
		return mapVMIDpmIP.get(vmID);
	}
	
	public static String getVMIDfromVMIP(String vmIP)
	{
		Iterator<String> it = mapVMIDIP.keySet().iterator();
		String key = "";
		
		while(it.hasNext())
		{
			key = it.next();
			
			if(mapVMIDIP.get(key).equals(vmIP))
				break;	
				
			if(!it.hasNext())
				key = null;
		}
		return key; // vmID
	}
	
	public static String getIDfromIP(String ip)
	{
		Iterator<String> it = mapIDIP.keySet().iterator();
		String key = "";
		
		while(it.hasNext())
		{
			key = it.next();
			
			if(mapIDIP.get(key).equals(ip))
				break;	
				
			if(!it.hasNext())
				key = null;
		}
		
		
		return key; // ID
	}
	
	public static String getPMIPfromVMIP(String IP)
	{
		String vmID = getVMIDfromVMIP(IP);
		String pmIP = "";
		boolean flag = false;
		
		if(vmID == null)
			pmIP = null;
		
		else
		{
			pmIP = getPMIPfromVMID(vmID);
			flag = true;
		}	
		
		if(!flag)
		{
			for(int i=0; pmIPs.size()> i; i++)
			{
				if(pmIPs.get(i).equals(IP))
				{
					pmIP = IP;
					break;
				}
			}		
		}
		
		return pmIP; 
	}
	
	public static ArrayList<String> getMyVMs(String pmIP)
	{
		ArrayList<String> vmIPlist = new ArrayList<String>();

		for(int i=0; vmIDs.size() > i; i++)
		{		
			if(vmIDs.get(i).contains(pmIP))
				vmIPlist.add(getVMIPfromVMID(vmIDs.get(i)));
		}
		
		return vmIPlist;			
	}
	
	public static ArrayList<String> getInsidePMfromIPforHistory(String ip) // for history
	{
		ArrayList<String> insidePMList = new ArrayList<String>();
		boolean pmFlag = false;
		
		for(int i=0; pmIPs.size() > i ; i++)
		{
			if(pmIPs.get(i).equals(ip))
			{
				pmFlag = true;
				break;
			}	
		}
		
		if(pmFlag)
		{
			insidePMList.add(ip); 
			insidePMList.addAll(getMyVMs(ip));				
		}	

		else
		{
			insidePMList.add(ip); // only show this VM's RT
		}

		return insidePMList; 
	}
	
	public static ArrayList<String> getInsidePMfromIP(String ip) // for group
	{
		ArrayList<String> insidePMList = new ArrayList<String>();
		boolean pmFlag = false;
		
		if(ip!=null)
		{
			insidePMList.add(ip); 
			insidePMList.addAll(getMyVMs(ip));			
		}

		return insidePMList; 
	}
	
	public static ArrayList<String> getMyNeighborVMs(String vmIP)
	{		
		ArrayList<String> neighborVMIPList = new ArrayList<String>();
		
//		db["10.1.0.20/10.214.10.15"].find({},{"neighbor.ip":1})
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query=new BasicDBObject();
		query.put("neighbor.ip",1);
		
		String vmID = getIDfromIP(vmIP);
		
		DBCursor cursor = cloudmanDB.getCollection(vmID).find(field,query);
		List<BasicDBObject> vmIPs = new ArrayList<BasicDBObject>();					
		while(cursor.hasNext())
		{			
			BasicDBObject result = (BasicDBObject)cursor.next();
					
			vmIPs = (List<BasicDBObject>)result.get("neighbor");
					
			for(int j=0; vmIPs.size() > j; j++)
			{
				BasicDBObject oneObject = vmIPs.get(j);
				String ip = oneObject.getString("ip");
					
				if(ip.equals(vmIP))
					continue;
					
				if(checkIfExist(ip))
					neighborVMIPList.add(ip);
			}
		}

		return neighborVMIPList;
	}
	
	public static boolean checkIfExist(String vm)
	{
		boolean exist = false;
		
		for(int i=0; allIPs.size() > i ; i++)
		{
			if(allIPs.get(i).equals(vm))
			{
				exist = true;
				break;
			}
		}
		
		return exist;
	}	
}
