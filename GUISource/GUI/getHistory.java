package GUI;

import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import com.mongodb.*;
import java.net.UnknownHostException;

public class getHistory
{
	static Mongo mongo;
	static DB historyDB;
	static DBCollection historyCollection;			
	
	static dbConnection db;
	
	static	ArrayList<String> timeType = new ArrayList<String>();
	static	ArrayList<String> utilType = new ArrayList<String>();
	
	static String historyRT = "";
	static String historyUtil = "";
	
	public getHistory(String dbIP, int dbPort, String dbName, String collName) throws MongoException 
	{
		try 
		{
			mongo = new Mongo(dbIP, dbPort);
			historyDB = mongo.getDB(dbName);
			historyCollection = historyDB.getCollection(collName);
			
			timeType.add("min");
			timeType.add("hour");
			timeType.add("day");
			timeType.add("month");
			timeType.add("year");
			
			utilType.add("CPU");
			utilType.add("RAM");
			utilType.add("Disk");
			utilType.add("Inbound");
			utilType.add("Outbound");
		} 
		catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}	
	}
	
	public getHistory() {}
	
	public static String getHistoryRT(String clickedIP)
	{
		historyRT = "[";
		Cursor cursor = getHistory(clickedIP);
		
		while(cursor.hasNext())
		{
			TreeMap<Integer, String> tsRT = new TreeMap<Integer, String>();
			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			String ip = (String)result.get("IP");
			
			for(int i=0; timeType.size() > i; i = i+1)
			{
				List<BasicDBObject> object = (ArrayList<BasicDBObject>)result.get(timeType.get(i));
				
				if(object == null)
					continue;
				
				for(int j=0; object.size() > j; j++)
				{
					String value = (String)object.get(j).get("RT");

					if(value == null)
						continue;
						
					int ts = (int)object.get(j).get("TS");
					tsRT.put(ts, value);
				}
			}
			
			Iterator<Integer> it = tsRT.keySet().iterator();			
			
			if(!it.hasNext())
			{
				historyRT += "{\"IP\" : \"" + ip + "\",";
				historyRT += "\"TS\" : \"" + (int)(System.currentTimeMillis()/1000) + "\",";
				historyRT += "\"RT\" : \"" + "0.0" + "\"}";
			}
			
			else
				while(it.hasNext())
				{
					int key = it.next();
					
					if(tsRT.get(key) == null || tsRT.get(key).equals("null"))
						continue;
						
					historyRT += "{\"IP\" : \"" + ip + "\",";
					historyRT += "\"TS\" : \"" + key + "\",";
					historyRT += "\"RT\" : \"" + tsRT.get(key) + "\"}";	
					
					if(it.hasNext())
						historyRT += ",";
				}	
			
			if(cursor.hasNext())
				historyRT += ",";
		}
			
		historyRT += "]";
		return historyRT;
	}
	
	public static String getHistoryUtil(String clickedIP)
	{		
		Cursor cursor = getHistory(clickedIP);
		
		while(cursor.hasNext())
		{
			TreeMap<Integer, String> tsUtil = new TreeMap<Integer, String>();
			ArrayList<String> types = new ArrayList<String>();
			
			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			String ip = "";
			
			for(int i=0; timeType.size() > i; i++)
			{
				List<BasicDBObject> object = (ArrayList<BasicDBObject>)result.get(timeType.get(i));
				
				if(object == null)
					continue;
				
				for(int j=0; object.size() > j; j++)
				{					
					for(int k=0; utilType.size() > k; k++)
					{
						String type = utilType.get(k);
						String value = (String)object.get(j).get(type);

						if(value != null)
						{							
							int ts = (int)object.get(j).get("TS");
							
							tsUtil.put(ts, value);
							types.add(type);
							break;
						}
					}
				}
			}
			
			Iterator<Integer> iter = tsUtil.keySet().iterator();			
			if(!iter.hasNext())
			{
				historyUtil = "[";

				ArrayList<String> lastUtil = getLastUtil(clickedIP);
				
				for(int k=0; utilType.size() >k; k++)
				{				
					historyUtil += "{\"Type\" : \"" + utilType.get(k) + "\",";
					historyUtil += "\"TS\" : \"" + (int)((System.currentTimeMillis()/1000)+(k-6)) + "\",";
					historyUtil += "\"Value\" : \"" + lastUtil.get(k) + "\"}";
					
					if(utilType.size() != k+1)
						historyUtil += ",";					
				}
			}
			
			else
			{		
/*				ArrayList<String> lastUtil = getLastUtil(clickedIP);
				
				for(int k=0; utilType.size() >k; k++)
				{
					tsUtil.put((int)(System.currentTimeMillis()/1000)+(k+1), lastUtil.get(k));
					types.add(utilType.get(k));					
				}			
*/				
				Iterator<Integer> it = tsUtil.keySet().iterator();				
				historyUtil = "[";
				
				int cnt = -1;
				while(it.hasNext())
				{
					int key = it.next();
					cnt++;
					
					if(tsUtil.get(key) == null || tsUtil.get(key).equals("null"))
						continue;
						
					historyUtil += "{\"Type\" : \"" + types.get(cnt) + "\",";
					historyUtil += "\"TS\" : \"" + key + "\",";
					historyUtil += "\"Value\" : \"" + tsUtil.get(key) + "\"}";	
					
					if(it.hasNext())
						historyUtil += ",";
				}	
			}
			if(cursor.hasNext())
				historyUtil += ",";
		}
			
		historyUtil += "]";
		
		return historyUtil;
	}
	
	public static ArrayList<String> getLastUtil(String ip)
	{
		ArrayList<String> lastUtilValues = new ArrayList<String>();
		ArrayList<String> historyGroup = new ArrayList<String>();
		historyGroup.add(ip);
		
		BasicDBObject query = new BasicDBObject();
		query.put("IP","$IP");
		
		for(int i=0; utilType.size() > i; i++)
		{
			String type = utilType.get(i);
			query.put(type,"$L"+type);
		}
		
		BasicDBObject project = new BasicDBObject("$project",query);	

		BasicDBObject match = new BasicDBObject("$match",new BasicDBObject("IP", new BasicDBObject("$in", historyGroup)));
				
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

		pipeline.add(project);	
		pipeline.add(match);				
			
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
		
		Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);
		
		System.out.println(" IP >> " + ip);	
		
		while(cursor.hasNext())
		{

			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			for(int i=0; utilType.size() > i; i++)
			{
				String type = utilType.get(i);
				String value = (String)result.get(type);	
				lastUtilValues.add(value);			
			}
		}
		
		return lastUtilValues;
	}
	
	public static Cursor getHistory(String ip) throws MongoException
	{		
		ArrayList<String> historyGroup = db.getInsidePMfromIPforHistory(ip);

		System.out.println("historyGroup >> " + historyGroup);
		
// db.history.aggregate({$project:{"IP":"$IP","min":"$MIN","hour":"$HOUR"}},{$match:{'IP':{$in:["10.214.10.15","10.1.0.21"]}}})
		BasicDBObject query = new BasicDBObject();		
		query.put("IP","$IP");
		query.put("min","$MIN");
		query.put("hour","$HOUR");
		query.put("day","$DAY");
		query.put("month","$MONTH");
		query.put("year","$YEAR");

		BasicDBObject project = new BasicDBObject("$project",query);	

		BasicDBObject match = new BasicDBObject("$match",new BasicDBObject("IP", new BasicDBObject("$in", historyGroup)));
				
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

		pipeline.add(project);	
		pipeline.add(match);				
			
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
		
		Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);	
		
		return cursor;
	}
	
	static String getDate(int time)
	{
		long milliseconds = time * 1000L;
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
       cal.setTimeInMillis(milliseconds);
	   
	   return sdf.format(cal.getTime());
	}
}
