/* Main class of Cloudman */

package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;
import java.net.UnknownHostException;

public class cloudmanGUI
{
// Set physical information about mongodb
	static String dbIP = null; // IP address
	static int dbPort; // Port number
	static String dbName = null; // DB name
	
	static String historyCollName = null; 
	
	// log db
	static String logDBIP = null;
	static int logDBPort;	
	static String logDBName = null;
	static String logCollName = null; 
	static String utilLogCollName = null; 
	static String rtCollName = null;
	static int logTimeInterval;

//	public static void main(String args[]){
	public cloudmanGUI() throws UnknownHostException{
		Configuration.setConfiguration();
		dbIP = Configuration.getDBIP();
		dbPort = Configuration.getDBPort();
		dbName = Configuration.getDBName();
		historyCollName = Configuration.getHistoryColl();
		
		dbConnection db = new dbConnection(dbIP, dbPort, dbName);
		
		utilRTCircles circle = new utilRTCircles();
		groupPacks group = new groupPacks();
		dependencyLinks link = new dependencyLinks();
		
		logDBIP = Configuration.getLogIP();
		logDBPort = Configuration.getLogPort();
	
		logDBName = Configuration.getLogName();
		logCollName = Configuration.getDependencyColl(); 
		utilLogCollName = Configuration.getUtilizationColl(); 
		rtCollName = Configuration.getResponsetimeColl();
	
		logTimeInterval = Configuration.getLogInterval();
		
		logAnalysis log = new logAnalysis(logDBIP, logDBPort, logDBName, logCollName, utilLogCollName, rtCollName, logTimeInterval);	
		ActionServlet servlet = new ActionServlet();
		getHistory history = new getHistory(dbIP, dbPort, dbName, historyCollName);		
		
/*		System.out.println("PM >> " + log.getChangedPMs());	

		System.out.println("Link 1 >> " + log.getChangedLinks());
		System.out.println("\n>> " + group.getGroup("10.1.0.42"));
		System.out.println(">> " + link.getLinks("10.1.0.42"));
		
		System.out.println("\n>> " + group.getGroup("10.1.0.13/10.1.0.13"));
		System.out.println(">> " + link.getLinks("10.1.0.13/10.1.0.13"));
		
		System.out.println("\n>> " + group.getGroup("10.1.0.21/10.1.0.21"));
		System.out.println(">> " + link.getLinks("10.1.0.21/10.1.0.21"));
*/
//		System.out.println("PM History >>  " + readHistory.getPMRT());	
//		System.out.println("VM History >>  " + readHistory.getVMRT());		
//		System.out.println("VMRT >>  " + historyRT.getVMRT());

//		System.out.println("Result 1 >> " + map.getMappedPMIP("10.8.8.22"));
//		System.out.println("Result 2 >> " + map.getMappedVMIP("10.1.0.43"));
//		rt.getPMRT();

//		System.out.println(history.getHistoryUtil("10.1.0.20"));
//		System.out.println(history.getHistoryUtil("10.214.10.15"));
	}
}

