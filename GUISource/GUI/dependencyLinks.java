package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;

public class dependencyLinks
{
	static dbConnection db;
	static groupPacks group;
	static String link = "";
	
	public static String getLinks(String clickedIP)
	{
		ArrayList<String> allVMlist = group.getAllVMlist();		
		HashSet<ArrayList<String>> dependencies = new HashSet<ArrayList<String>>();
//	db["10.1.0.20/10.214.10.15"].aggregate({$unwind:"$neighbor"},{$project:{_id:0,"vmIP":"$ip","neighborIP":"$neighbor.ip","type":"$neighbor.type","dependency":"$neighbor.dependency"}});

		for(int i=0; allVMlist.size()>i; i++)
		{
			String vmIP = allVMlist.get(i);
			String vmID = db.getIDfromIP(vmIP);
			DBCollection collection = db.getCollection(vmID);
			
			BasicDBObject query = new BasicDBObject();		
			query.put("_id",0);
			query.put("vmIP","$ip");
			query.put("neighborIP","$neighbor.ip");
			query.put("type","$neighbor.type");
			query.put("dependency","$neighbor.dependency");

			BasicDBObject project = new BasicDBObject("$project",query);	
			BasicDBObject unwind1 = new BasicDBObject("$unwind","$neighbor");	
			
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

			pipeline.add(unwind1);	
			pipeline.add(project);				
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
																
			Cursor cursor = collection.aggregate(pipeline,aggregationOptions);

			while(cursor.hasNext())
			{	
				ArrayList<String> oneDependency = new ArrayList<String>();
				
				BasicDBObject result = (BasicDBObject)cursor.next();
				
				String vm = result.getString("vmIP");
				String neighborIP = result.getString("neighborIP");
				String type = result.getString("type");
				String dependency = result.getString("dependency");
				
				if(type.equals("server"))
				{
					oneDependency.add(vm); // source
					oneDependency.add(neighborIP); // destination
					oneDependency.add(dependency); // type;
				}
					
				else 
				{
					oneDependency.add(neighborIP); // source
					oneDependency.add(vm); // destination
					oneDependency.add(dependency); // type;
				}	
						
				dependencies.add(oneDependency);
			}
		}
						
		link = "{\"links\":[";
			
		Iterator<ArrayList<String>> it = dependencies.iterator();
		
		while(it.hasNext())
		{
			ArrayList<String> oneDependency = new ArrayList<String>();
			oneDependency = it.next();

			int inCnt = 0;
				
			link += "{";
				
			while(oneDependency.size() > inCnt)
			{
				switch(inCnt)
				{
					case 0: link += "\"source\":";
						break;
					case 1: link += "\"target\":";
						break;		
					case 2: link += "\"type\":";
						break;

					default: link += "";
				}
				link += "\"" + oneDependency.get(inCnt) + "\"";
				inCnt++;
					
				if(inCnt != oneDependency.size())
					link += ",";
			}
				
			link += "}";
				
			if(it.hasNext())
				link += ",";
		}
		link += "]}";
		return link;
	}

}