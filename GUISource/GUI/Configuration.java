package GUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class Configuration {
    private static String dbIP = null;
    private static int dbPort;
	private static String dbName=null;
	
	private static String logIP = null;
    private static int logPort;
	private static String logName=null;
	private static int logInterval; // second
	
	private static String dependencyColl=null;
	private static String utilizationColl=null;
	private static String responsetimeColl=null;
	private static String historyColl=null;
		
	public static void setConfiguration()
	{
		try
		{
//			File readFile = new File("/usr/share/tomcat/webapps/ROOT/cloudman/Config.txt");
			File readFile = new File("C://Program Files (x86)//Apache Software Foundation//Tomcat 8.0//lib//GUI//Config.txt");
			BufferedReader inFile = new BufferedReader(new FileReader(readFile));
			String sLine = null;
			
			while((sLine = inFile.readLine()) != null)
			{
				String parameter = sLine.split("=")[0];
				String value = sLine.split("=")[1];
				
				if(parameter.equals("DB IP address"))
					dbIP = value;
				
				else if(parameter.equals("DB port"))
					dbPort = Integer.parseInt(value);
				
				else if(parameter.equals("DB name"))
					dbName = value;
			
				else if(parameter.equals("log DB IP address"))
					logIP = value;
				
				else if(parameter.equals("log DB port"))
					logPort = Integer.parseInt(value);
				
				else if(parameter.equals("log DB name"))
					logName = value;
					
				else if(parameter.equals("time interval of logging"))
					logInterval = Integer.parseInt(value);		
				
				else if(parameter.equals("dependency log"))
					dependencyColl = value;
				
				else if(parameter.equals("utilization log"))
					utilizationColl = value;
				
				else if(parameter.equals("response time log"))
					responsetimeColl = value;
										
				else if(parameter.equals("history collection"))
					historyColl = value;
					
				else if(parameter.equals("history collection"))
					historyColl = value;

				sLine = null;
			}
			inFile.close();
		}
		catch(Exception ex)
		{
	    	ex.printStackTrace();
		}
	}	
    
    public static String getDBIP(){
    	return dbIP;
    }
    
    public static int getDBPort(){
    	return dbPort;
    }
	
    public static String getDBName(){
    	return dbName;
    }

    public static String getLogIP(){
    	return logIP;
    }
    
    public static int getLogPort(){
    	return logPort;
    }
	
    public static String getLogName(){
    	return logName;
    }	
	
	public static int getLogInterval(){
    	return logInterval;
    }
	
	public static String getDependencyColl(){
    	return dependencyColl;
    }

    public static String getUtilizationColl(){
    	return utilizationColl;
    }
	
	public static String getResponsetimeColl(){
    	return responsetimeColl;
    }

    public static String getHistoryColl(){
    	return historyColl;
    }
}
