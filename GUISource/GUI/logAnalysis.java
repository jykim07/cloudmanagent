package GUI;

import com.mongodb.*;
import java.util.*;

public class logAnalysis
{	
	static cloudmanGUI cloudGui;
	static dbConnection dbConnect;
	static groupPacks gPacks;
	static utilRTCircles circles;

	static int logTimeInterval;
	
	static HashSet<String> changedPM = new HashSet<String>();	

	static String changedPMIP = "";
	
	static ArrayList<ArrayList<String>> changedLink = new ArrayList<ArrayList<String>>();
	static String changedLinkIP = "";
	
	static ArrayList<ArrayList<String>> hashChangedLinks = new ArrayList<ArrayList<String>>();
	
	static HashSet<String> exist = new HashSet<String>();
	static HashSet<String> nonExist = new HashSet<String>();
	
	static ArrayList<String> changedNeighbors = new ArrayList<String>();
	
	static ArrayList<String> deadPM = new ArrayList<String>();
	static String deadPMIP = "";	

	static Mongo logMongo;
	static DB logDB;
	static DBCollection logCollection;
	static DBCollection utilLogCollection;
	static DBCollection rtLogCollection;
							   
	static long oplogQueryTS = 0;  // 1000000000, 1430419645
	
	public logAnalysis(String logDBIP, int logDBPort, String logDBName, String logCollName, String utilLogCollName, String rtCollName, int logTimeInterval) throws MongoException
	{	
		try {
				logMongo = new Mongo(logDBIP, logDBPort);
				logDB = logMongo.getDB(logDBName);
				logCollection = logDB.getCollection(logCollName);
				utilLogCollection = logDB.getCollection(utilLogCollName);
				rtLogCollection = logDB.getCollection(rtCollName);
				this.logTimeInterval = logTimeInterval;
			
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public static long getNextQueryTS()
	{
		oplogQueryTS = System.currentTimeMillis() / 1000L;
		
		Date nextQueryTS = new Date((oplogQueryTS-logTimeInterval)*1000L); 		
		long queryNextTS = (long)nextQueryTS.getTime()/1000;

		return queryNextTS;
	}
	
	public static HashSet<String> traceChangedPM()
	{		
		HashSet<String> changedPM = new HashSet<String>();

//db.cloudmanLog.find({ts:{$gte:1431708680},'VM.ip':{$exists:true}})
	
		BasicDBObject field = new BasicDBObject();
		BasicDBObject query1 = new BasicDBObject();
		BasicDBObject query2 = new BasicDBObject();
		BasicDBObject query3 = new BasicDBObject();

		query1.put("$exists",true);		
		query2.put("$gte",getNextQueryTS()); 

		field.put("ts",query2);			
		field.put("VM.ip",query1);
		
		DBCursor cursor = logCollection.find(field);
		
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			String PMIP = result.getString("PM");		
			changedPM.add(PMIP);
		}

		return changedPM;
	}
	
	public static String getChangedPMs()
	{
		changedPM.clear();
		ArrayList<String> changedNeighborPMs = new ArrayList<String>();
		ArrayList<String> remove = new ArrayList<String>();

		changedPMIP = "";
		changedPMIP += "[";	
		
		hashChangedLinks = traceChangedLinks();
		
		System.out.println("traceChangedLinks() : " + hashChangedLinks);
		changedPM.addAll(traceChangedPM());
		
		System.out.println("Exist : " + exist);
		System.out.println("nonExist : " + nonExist);
		remove.addAll(nonExist);
		
		for(int i=0; remove.size() > i; i++)
		{
			Iterator<String> it = changedPM.iterator();
			
			while(it.hasNext())
			{
				String pm = it.next();
				
				System.out.println("pm >> " + pm + " remove >> " + remove.get(i)); 
				if(remove.get(i).equals(pm))
				{
					it.remove();	
					break;
				}
			}
		}
		
		changedNeighborPMs = getNeighborPMsofChangedPMs(changedPM);
		changedPM.addAll(changedNeighborPMs);
		
		Iterator<String> it = changedPM.iterator();
		
		while(it.hasNext())
		{
			changedPMIP += "\""+ it.next() +"\"";

			if(it.hasNext())
				changedPMIP += ",";
		}
		
		changedPMIP += "]";
		
		return changedPMIP;
	}
	
	public static ArrayList<ArrayList<String>> traceChangedLinks()
	{				
//db.cloudmanLog.aggregate({$match:{"ts":{"$gte":1430419645}}},{$project:{"vmIP":"$VM.ip","added":"$VM.new","deleted":"$VM.old","changed":"$VM.change","ts":"$ts"}}).pretty()
		changedNeighbors.clear();
		changedLink.clear();
		
		BasicDBObject query = new BasicDBObject();		
		query.put("vmIP","$VM.ip");
		query.put("added","$VM.new");
		query.put("deleted","$VM.old");
		query.put("changed","$VM.change");
		query.put("ts","$ts");
		
		BasicDBObject matchQuery = new BasicDBObject("ts",new 	BasicDBObject("$gte",getNextQueryTS())); //getNextQueryTS()
		
		BasicDBObject project = new BasicDBObject("$project",query);		
		BasicDBObject match = new BasicDBObject("$match",matchQuery);
		
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		pipeline.add(match);
		pipeline.add(project);	
		
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
															
		Cursor cursor = logCollection.aggregate(pipeline,aggregationOptions);
		
		String lastTS = "";
		String nextTS = "";

		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject)cursor.next();	
			
			String vmIP = (String)result.get("vmIP");			
				
			ArrayList<BasicDBObject> addedIPs = (ArrayList<BasicDBObject>)result.get("added");
			ArrayList<BasicDBObject> deletedIPs = (ArrayList<BasicDBObject>)result.get("deleted");			
			ArrayList<BasicDBObject> changedIPs = (ArrayList<BasicDBObject>)result.get("changed");					
				
			for(BasicDBObject embedded1: addedIPs)
			{
				if(embedded1.size() == 0)
					break;
				
				ArrayList<String> tempLink = new ArrayList<String>();
					
				String neighborIP = (String)embedded1.get("ip");
				String type = (String)embedded1.get("type");
				String opType = "added";
					
				changedNeighbors.add(neighborIP);
						
				if(type.equals("server"))
				{
					tempLink.add(vmIP);
					tempLink.add(neighborIP);
					tempLink.add(opType);		
				}
					
				else
				{
					tempLink.add(neighborIP);
					tempLink.add(vmIP);
					tempLink.add(opType);					
				}
				
				changedLink.add(tempLink);
			}
			
			for(BasicDBObject embedded2: deletedIPs)
			{
				if(embedded2.size() == 0)
					break;			
				
				ArrayList<String> tempLink = new ArrayList<String>();
					
				String neighborIP = (String)embedded2.get("ip");
				String type = (String)embedded2.get("type");
				String opType = "deleted";
					
				changedNeighbors.add(neighborIP);
		
				if(type.equals("server"))
				{
					tempLink.add(vmIP);
					tempLink.add(neighborIP);
					tempLink.add(opType);		
				}
					
				else
				{
					tempLink.add(neighborIP);
					tempLink.add(vmIP);
					tempLink.add(opType);					
				}
				
				changedLink.add(tempLink);
			}
			
			for(BasicDBObject embedded3: changedIPs)
			{
				if(embedded3.size() == 0)
					break;
				ArrayList<String> tempLink = new ArrayList<String>();
					
				String neighborIP = (String)embedded3.get("ip");
				String type = (String)embedded3.get("type");
				String opType = "changed";
					
				changedNeighbors.add(neighborIP);
								
				if(type.equals("server"))
				{
					tempLink.add(vmIP);
					tempLink.add(neighborIP);
					tempLink.add(opType);		
				}
					
				else
				{
					tempLink.add(neighborIP);
					tempLink.add(vmIP);
					tempLink.add(opType);					
				}
				
				changedLink.add(tempLink);
			}
		}
		
		ArrayList<ArrayList<String>> hashChangedLinks = new ArrayList<ArrayList<String>>(new HashSet<ArrayList<String>>(changedLink));
			
		checkChangedPMs(hashChangedLinks);	

		return hashChangedLinks;		
	}
	
	public static String getChangedLinks()
	{		
		changedLinkIP = "{\"links\":[";
					
		int cnt=0; // PM1, PM2, ....
		
		while(hashChangedLinks.size() > cnt)
		{
			ArrayList<String> getOneLinkArray = hashChangedLinks.get(cnt);
			
			int inCnt = 0;
			changedLinkIP += "{";			
			
			while(getOneLinkArray.size() > inCnt)
			{
				switch(inCnt)
				{
					case 0: changedLinkIP += "\"source\":";
						break;
					case 1: changedLinkIP += "\"target\":";
						break;		
					case 2: changedLinkIP += "\"type\":";
						break;

					default: changedLinkIP += "";
				}
				changedLinkIP += "\"" + getOneLinkArray.get(inCnt) + "\"";
								
				inCnt++;				
	
				if(inCnt != getOneLinkArray.size())
					changedLinkIP += ",";
			}				
			
			changedLinkIP += "}";		
			
			cnt++;
			
			if(hashChangedLinks.size() != cnt)
				changedLinkIP += ",";			
		}
		
		changedLinkIP += "]}";
		
		return changedLinkIP;
	}
	
	public static void checkChangedPMs(ArrayList<ArrayList<String>> changedLinks)
	{	
		exist.clear();
		nonExist.clear();
		for(int i=0; changedLinks.size() > i; i++)
		{
			ArrayList<String> oneLink = changedLinks.get(i);	
			System.out.println("oneLink >> " + oneLink);
			
			String source =dbConnect.getPMIPfromVMIP(oneLink.get(0));
			String destination = dbConnect.getPMIPfromVMIP(oneLink.get(1));
			
			System.out.println(source + " >> " + destination);
			
			if(source == null && destination == null)
				continue;
				
			else
			{
				if(source==null)
					nonExist.add(destination);
			
				else if(destination==null)
					nonExist.add(source);
				
				else
				{
					exist.add(source);
					exist.add(destination);
				}
			}
		}			
		
		Iterator<String> itExist = exist.iterator();
		
		while(itExist.hasNext())
		{
			Iterator<String> itNonExist = nonExist.iterator();
			String existElem = itExist.next();
			while(itNonExist.hasNext())
			{
				String nonElem = itNonExist.next();			

				if(existElem.equals(nonElem))
				{
					itNonExist.remove();
					break;
				}		
			}
		}
	}
	
	public static String traceDeadPMs()
	{
		ArrayList<String> tempDeadPM = new ArrayList<String>();
		
		tempDeadPM.clear();
		
// db.cloudmanLog.find({ts:{$gte:1431708680},'status':{$exists:true}})	
		BasicDBObject field = new BasicDBObject();
		BasicDBObject query1 = new BasicDBObject();	
		BasicDBObject query2 = new BasicDBObject();		
		
		query1.put("$exists",true);		
		query2.put("$gte",getNextQueryTS());
		
		field.put("status",query1);	
		field.put("ts",query2);
			
		DBCursor cursor = utilLogCollection.find(field);
		
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject) cursor.next();	
			
			String valStatus = result.getString("status");
			String PMIP = result.getString("PM");		

			if(valStatus.equals("dead"))
				tempDeadPM.add(PMIP);
		}

		deadPM = new ArrayList<String>(new HashSet<String>(tempDeadPM));
		return deadPMIP;
	}
	
	public static String getDeadPMs()
	{		
		deadPMIP = "";
		deadPMIP += "[";	
		
		traceDeadPMs();
		
		int cnt=0; // PM1, PM2, ....
		
		while(deadPM.size() > cnt)
		{
			deadPMIP += "\""+deadPM.get(cnt)+"\"";
			cnt++;
			
			if(deadPM.size() != cnt)
				deadPMIP += ",";
		}
		
		deadPMIP += "]";
		
		return deadPMIP;
	}
	
	public static ArrayList<String> getNeighborPMsofChangedPMs(HashSet<String> changedPMs)
	{
		HashSet<String> neighborPMs = new HashSet<String>();
		
		Iterator<String> it = changedPMs.iterator();
		
		while(it.hasNext())
		{		
			String pm = it.next();
			LinkedHashMap<String,ArrayList<String>> mapPMVMlist = gPacks.getMapPMVMlist(pm);
			
			Iterator<String> key = mapPMVMlist.keySet().iterator();		
			
			while(key.hasNext())
			{
				String pmIP = key.next();	
				neighborPMs.add(pmIP);
			}
		}
		
		ArrayList<String> neighborPMlist = new ArrayList<String>();
		neighborPMlist.addAll(neighborPMs);
		
		return neighborPMlist;
	}
		
	
	public static String getUpdatedRT()
	{
		HashMap<String,String> updatedRTMap = new HashMap<String, String>();
		String updatedRT = "";
		
		//db.responseTimeLog.find({'PM':{$exists:true}})
		BasicDBObject field = new BasicDBObject();
		BasicDBObject query1 = new BasicDBObject();
		BasicDBObject query2 = new BasicDBObject();

		query1.put("$exists",true);		
		query2.put("$gte",getNextQueryTS()); 

		field.put("ts",query2);			
		field.put("PM",query1);
		
		DBCursor cursor = rtLogCollection.find(field);
	
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject) cursor.next();				

			String ipPM = result.getString("PM");
			String rtPM = result.getString("rt");
			
			updatedRTMap.put(ipPM,rtPM);
		}
		
		Iterator <String> it = updatedRTMap.keySet().iterator();
		int cnt = 0;
		
		updatedRT = "[";
		while(it.hasNext())
		{
//		[{"pm" : "10.1.0.41","rt" : "200.0"},{"pm" : "10.1.0.43","rt" : "200.0"}]
			updatedRT += "{";
			String pm = it.next();
			String rt = updatedRTMap.get(pm);
			
			updatedRT += "\"pm\" : \"" + pm + "\",";
			updatedRT += "\"rt\" : \"" + rt + "\"";
			updatedRT += "}";
			
			cnt++;
			
			if(updatedRTMap.size() != cnt)
				updatedRT += ",";
		}
		updatedRT += "]";
		
		System.out.println("\n============= Updated RT ==============");
		System.out.println(updatedRT);
		
		return updatedRT;
	
	}
	
	public static String getChangedUtils()
	{				
		ArrayList<ArrayList<String>> changedUtil = new ArrayList<ArrayList<String>>();
		
//db.cloudmanLog.find({ts:{$gte:1431708680},'VM.ip':{$exists:true}})
		BasicDBObject field = new BasicDBObject();
		BasicDBObject query1 = new BasicDBObject();
		BasicDBObject query2 = new BasicDBObject();
		BasicDBObject query3 = new BasicDBObject();

		query1.put("$exists",true);		
		query2.put("$gte",getNextQueryTS()); 

		field.put("ts",query2);			
		field.put("PM",query1);
		
		DBCursor cursor = utilLogCollection.find(field);
		
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject) cursor.next();				

			String PMIP = result.getString("PM");
			String cpu = result.getString("cpu");
			String mem = result.getString("mem");
			String disk = result.getString("disk");
			String net_in = result.getString("netin");
			String net_out = result.getString("netout");

			if(changedUtil.size()==0)
			{
				ArrayList<String> onePMUtil = new ArrayList<String>();
				
				onePMUtil.add(PMIP);
				onePMUtil.add(cpu);			
				onePMUtil.add(mem);
				onePMUtil.add(disk);	
				onePMUtil.add(net_in);
				onePMUtil.add(net_out);

				changedUtil.add(onePMUtil);					
			}
			
			else
			{
				int cnt2 = 0;
				int foundFlag = 0;
				
				while(changedUtil.size() > cnt2)
				{
					if(changedUtil.get(cnt2).get(0).equals(PMIP))
					{					
						foundFlag = 1;		
						break;
					}
				
					else
						cnt2++;
				}
				
				if(foundFlag ==1)
				{
					ArrayList<String> onePMUtil = changedUtil.get(cnt2);	
					
					if(cpu!=null)
						onePMUtil.set(1,cpu);
					
					else if(mem!=null)
						onePMUtil.set(2,mem);	

					else if(disk!=null)
						onePMUtil.set(3,disk);
						
					else if(net_in!=null)
						onePMUtil.set(4,net_in);	

					else if(net_out!=null)
						onePMUtil.set(5,net_out);	
					
					changedUtil.set(cnt2,onePMUtil);	
				}		

				else
				{
					ArrayList<String> onePMUtil = new ArrayList<String>();		
					
					onePMUtil.add(PMIP);
					onePMUtil.add(cpu);			
					onePMUtil.add(mem);
					onePMUtil.add(disk);
					onePMUtil.add(net_in);
					onePMUtil.add(net_out);				
				
					changedUtil.add(onePMUtil);					
				}
			}			
		}
		
		int cnt = 0;
		String strUtil = "["; 

		while(changedUtil.size() > cnt)
		{ 
			// [{ip: "10.1.0.41", cpu: "1.21", mem: "97.57", disk: "36.73", net_in: "14.8", net_out: "0.0"},{ip: "10.1.0.43", cpu: "3.4", mem: "96.79", disk: "18.37", net_in: "36.8", net_out: "0.0"}]
			ArrayList<String> temp = changedUtil.get(cnt);			
			
			strUtil += "{";
			
			strUtil += "\"ip\" : \"" + temp.get(0) + "\",";
			strUtil += "\"cpu\" : \"" + temp.get(1) + "\",";
			strUtil += "\"mem\" : \"" + temp.get(2) + "\",";
			strUtil += "\"disk\" : \"" + temp.get(3) + "\",";
			strUtil += "\"net_in\" : \"" + temp.get(4) + "\",";
			strUtil += "\"net_out\" : \"" + temp.get(5) + "\"";
			
			strUtil += "}";

			cnt++;
			
			if(changedUtil.size() != cnt)
				strUtil += ",";
		}
		
		strUtil += "]";
		
		return strUtil;
	}
}