package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;

public class groupPacks
{
	static dbConnection db;
	
	static String group = "";
	static double vmMinSize = 100; 
	
	static ArrayList<String> allVMlist = new ArrayList<String>(); 

	public static ArrayList<String> getAllVMlist()
	{
		return allVMlist;
	}

	public static LinkedHashMap<String,ArrayList<String>> getMapPMVMlist(String pmIP)
	{
		LinkedHashMap<String,ArrayList<String>> mapPMVMlist = new LinkedHashMap<String,ArrayList<String>>();
		
		ArrayList<String> insidePMList = db.getInsidePMfromIP(pmIP);

		mapPMVMlist.put(pmIP, insidePMList);	
		
		HashSet<String> new_allNeighbors = new HashSet<String>();
		HashSet<String> old_allNeighbors = new HashSet<String>();
				
		// Find my VMs' neighbors
		Set<String> myVMsNeighbors = getMyVMsNeighbors(insidePMList);						
		// Get the neighbors' PMs and VMs
		Iterator<String> result = myVMsNeighbors.iterator();		
			
		while(result.hasNext())
		{
			String pm = db.getPMIPfromVMIP(result.next());

			if(pm == null)
				continue;
			
			insidePMList = db.getInsidePMfromIP(pm);
			mapPMVMlist.put(pm, insidePMList);	

			myVMsNeighbors = getMyVMsNeighbors(insidePMList);
			old_allNeighbors.addAll(myVMsNeighbors);
		}

		while(true)
		{	
			Iterator<String> itOld = old_allNeighbors.iterator();
			HashSet<String> update = new HashSet<String>();
			
			while(itOld.hasNext())
			{
				String oldOne = itOld.next();
				Iterator<String> itNew = new_allNeighbors.iterator();
				
				while(itNew.hasNext())
				{
					String newOne = itNew.next();
					
					if(oldOne.equals(newOne))
					{
						itOld.remove();
						break;
					}						
				}
			}
			
			update.addAll(old_allNeighbors);
			new_allNeighbors.addAll(update);
			old_allNeighbors.addAll(new_allNeighbors);			
					
			result = update.iterator();		
	
			while(result.hasNext())
			{
				String pm = db.getPMIPfromVMIP(result.next());
				if(pm == null)
					continue;

				insidePMList = db.getInsidePMfromIP(pm);
				
				mapPMVMlist.put(pm, insidePMList);
				
				myVMsNeighbors = getMyVMsNeighbors(insidePMList);
				old_allNeighbors.addAll(myVMsNeighbors);
			}
			
			if(old_allNeighbors.equals(new_allNeighbors))
				break;
		}
		return mapPMVMlist;
	}
	
	public static String getGroup(String clickedIP)
	{
		allVMlist.clear();
		
		String group = "";
		
		LinkedHashMap<String,ArrayList<String>> mapPMVMlist = getMapPMVMlist(clickedIP);
		
		ArrayList<String> insidePMList = new ArrayList<String>();
		
		if(mapPMVMlist.size()==1)
		{	
			Iterator<String> it = mapPMVMlist.keySet().iterator();
			String pmIP = it.next();	

			insidePMList = mapPMVMlist.get(pmIP);
			
			// This PM has no VM
			if(insidePMList.size()==0) 
				group += "{\"name\" : " + "\"" + pmIP + "\", \"children\" : []}";
				
			// This PM has no neighbor
			else
			{
				group += "{\"name\" : " + "\"" + pmIP + "\", \"children\" : ["; 

				int cnt=0;		

				System.out.println("inside PMList >> " + insidePMList);
				
				while(insidePMList.size() > cnt)
				{
					group += "{\"name\" : " + "\"";
					group += insidePMList.get(cnt);
					group += "\", \"size\" :";
					group += getVMSize(insidePMList.get(cnt));
					group += "}";
					
					cnt++;
					
					if(cnt != insidePMList.size())
						group += ",";
				}
				group += "]}";
			}
		}
			
		// This PM has more than one neighbor
		else	
		{
			Iterator<String> it = mapPMVMlist.keySet().iterator();
			
			group += "{\"name\" : " + "\"" + "group" + "\", \"children\" : ["; 
						
			while(it.hasNext())
			{
				String pmIP = it.next();
				insidePMList = mapPMVMlist.get(pmIP);
				
				group += "{\"name\" : " + "\"" + pmIP + "\", \"children\" : [";
				
				int cnt = 0;
				while(insidePMList.size() > cnt)
				{
					group += "{\"name\" : " + "\"";
					group += insidePMList.get(cnt);
					group += "\", \"size\" :";
					group += getVMSize(insidePMList.get(cnt));
					group += "}";
					
					allVMlist.add(insidePMList.get(cnt)); // for the dependency
						
					cnt++;
					if(cnt != insidePMList.size())
						group += ",";
				}
				group += "]}";
				
				if(it.hasNext())
					group += ",";
			}	
			group += "]}";			
		}
	
		return group;
	}
		
		
	public static Set<String> getMyVMsNeighbors(ArrayList<String> myVMIPlist)
	{
		Set<String> neighborIPs = new HashSet<String>();
		
		// Find my VMs' neighbor VMs
		for(int i=0; myVMIPlist.size() > i; i++)
		{
			ArrayList<String> myNeighborVMs = db.getMyNeighborVMs(myVMIPlist.get(i));
			
			if(myNeighborVMs == null)
				continue;	
			
			neighborIPs.addAll(myNeighborVMs);
		}
		
		return neighborIPs;
	}
	
	public static int getVMSize(String vmIP)
	{
//		String vmID = db.getVMIDfromVMIP(vmIP);
		String vmID = db.getIDfromIP(vmIP);
		DBCollection collection = db.getVMCollection(vmID);
		
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query=new BasicDBObject();
		query.put("response_time",1);
		
		DBCursor cursor = collection.find(field,query);
		BasicDBObject result = (BasicDBObject)cursor.next();
		
		String rt = result.getString("response_time");
		double vmSize = 0;
		
		if(rt != null)
			vmSize = Double.parseDouble(rt);
		
		if(vmSize == 0)
			vmSize = vmMinSize;
			
		return (int)vmSize;		
	}
}
