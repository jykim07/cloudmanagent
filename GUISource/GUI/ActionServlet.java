/* Receive requests from users and interact with "Model" */

package GUI;

import java.io.IOException;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ActionServlet
 */

public class ActionServlet extends HttpServlet {
 private static final long serialVersionUID = 1L;
    utilRTCircles circlePM;
	groupPacks packPM;
	dependencyLinks linkPM;
	logAnalysis logAnal;
	getHistory history;
	
    static String name="";
    public ActionServlet(){  
	}

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  response.setContentType("text/plain");  
  response.setCharacterEncoding("UTF-8"); 
  response.setHeader("Pragma","No-cache");
  response.setDateHeader("Expires",-1);
  response.setHeader("Cache-Control","no-Cache"); 

// Response PM data including utilization  
  if(!request.getParameter("circlePM").toString().equals("")){
	name = circlePM.getUtilRT(); 	
	response.getWriter().write(name); 
  }
 

// Response group data  
  else if(!request.getParameter("packPM").toString().equals("")){
	name = packPM.getGroup(request.getParameter("packPM").toString()); System.out.println("Pack PM : " + name);
	response.getWriter().write(name); 
  }

// Response dependency data  
  else if(!request.getParameter("linkPM").toString().equals(""))
  {
 	name = linkPM.getLinks(request.getParameter("linkPM").toString()); System.out.println("Link PM : " + name);
	response.getWriter().write(name); 
  }

// Response updated PMs
  else if(!request.getParameter("changedPM").toString().equals(""))
  {
 	name = logAnal.getChangedPMs(); 
	response.getWriter().write(name); 
  }

// Response updated dependency data  
  else if(!request.getParameter("changedLink").toString().equals(""))
  {
 	name = logAnal.getChangedLinks();
	response.getWriter().write(name); 
  }

// Response dead PMs  
  else if(!request.getParameter("deadPMs").toString().equals(""))
  {
 	name = logAnal.getDeadPMs();
	response.getWriter().write(name); 
  }

// Response updated utilization  
  else if(!request.getParameter("changedUtil").toString().equals(""))
  {
 	name = logAnal.getChangedUtils();
	response.getWriter().write(name); 
  }
  
  // Response updated response time
  else if(!request.getParameter("changedRT").toString().equals(""))
  {
 	name = logAnal.getUpdatedRT();
	response.getWriter().write(name); 
  }

  // Response RT history of a clicked machine 
  else if(!request.getParameter("historyRT").toString().equals("")){
	name = history.getHistoryRT(request.getParameter("historyRT").toString()); 
	response.getWriter().write(name); 
  }
  
   // Response utilization history of a clicked machine 
  else if(!request.getParameter("historyUtil").toString().equals("")){
	name = history.getHistoryUtil(request.getParameter("historyUtil").toString()); 
	response.getWriter().write(name); 
  }
 }

 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  // TODO Auto-generated method stub
  
 }

}