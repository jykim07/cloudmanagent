package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;
import java.net.UnknownHostException;

public class utilRTCircles
{
	static dbConnection db;
	
	static ArrayList<String> utilType = new ArrayList<String>();
	static ArrayList<String> utilValue = new ArrayList<String>();
	
	static String strResult = "";

	static BasicDBObject field = new BasicDBObject();	
	static BasicDBObject query = new BasicDBObject();
	
	static ArrayList<String> IDs = new ArrayList<String>();	
	static ArrayList<String> IPs = new ArrayList<String>();	
	static ArrayList<DBCollection> pmCollections = new ArrayList<DBCollection>();	
	
	public static void setUtilAttr()
	{
		utilType.clear();
		utilValue.clear();
		
		utilType.add("cpu");
		utilType.add("mem");
		utilType.add("disk");
		utilType.add("net_in");
		utilType.add("net_out");
	}
	
	public static String getResult(int index)
	{		
		String str = "";				
		
		BasicDBObject query = new BasicDBObject();		
		BasicDBObject field = new BasicDBObject();		
		
		query.put("_id",0);
		query.put("id","$id");
		query.put("ip","$ip");
		query.put("cpu","$util.cpu");
		query.put("mem","$util.mem");		
		query.put("disk","$util.disk");
		query.put("net_in","$util.net_in");		
		query.put("net_out","$util.net_out");
		query.put("RT","$response_time");

		BasicDBObject project = new BasicDBObject("$project",query);	
			
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		pipeline.add(project);				
		
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
			
		Cursor cursor = pmCollections.get(index).aggregate(pipeline,aggregationOptions);			
		
		BasicDBObject result = (BasicDBObject)cursor.next();
		String id = IDs.get(index).split("/")[0] + "/" + IDs.get(index).split("/")[1];
		
		str += "\"id\" : \"" + id + "\",";
		str += "\"ip\" : \"" + IPs.get(index) + "\",";
		
		int numType=0;
		while(utilType.size() > numType)
		{		
			String utilResult = result.getString(utilType.get(numType));
			
			str += "\"" + utilType.get(numType) + "\" : \"" + utilResult + "\"";
			str += " , ";
			
			numType++;		
		}
		
		str += "\"rt\" : \"" + result.getString("RT") + "\"";
		
		return str;
	}
	
	// Return PM data including utilization and RT
	public static String getUtilRT()
	{
		IDs = db.getPMIDs();
		IPs = db.getPMIPs();
		pmCollections = db.getPMCollections();
		
		strResult = "[";
	
		setUtilAttr();	
		for(int i=0; IPs.size() > i ; i++)
		{
			strResult += "{";
			strResult += getResult(i);
			strResult += "}";
			
			if(IPs.size() != i+1)
				strResult += ",";
		}

		strResult += "]";
		return strResult;
	}
}
