package GUI;

import com.mongodb.*;
import java.util.*;

public class getMapTable extends Thread 
{		
//	static String dbIP = "127.0.0.1";
//	static int dbPort = 27017;
	
//	static String dbName = "jy";
//	static String collName = "pmvmMapTable"; 

	static Mongo mongo;
	static DB db;
	static DBCollection mapCollection;	

	static String prefixPM = ""; // PM's name for utilization
	static String prefixPMT = "";	// PM's name for VMs(neighbors)
	
	static ArrayList<DBCollection> collectionPM = new ArrayList<DBCollection>();
	static ArrayList<DBCollection> collectionPMT = new ArrayList<DBCollection>();	
	static ArrayList<DBCollection> collectionAll = new ArrayList<DBCollection>();	
	
	static ArrayList<String> PMTcollectionIPs = new ArrayList<String>();
	static ArrayList<String> PMcollectionIPs = new ArrayList<String>();	
	static ArrayList<String> allCollectionIPs = new ArrayList<String>();
	
	static HashMap<String,String> mapTableVMPM = new HashMap<String,String>();
		
	public getMapTable(String dbIP, int dbPort, String dbName, String mapName, String prefixPM, String prefixPMT) throws MongoException 
	{	
		try {
				mongo = new Mongo(dbIP, dbPort);
				db = mongo.getDB(dbName);
				mapCollection = db.getCollection(mapName);
				
				this.prefixPM = prefixPM;
				this.prefixPMT = prefixPMT;
				
		} 
		catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public getMapTable()
	{
		setCollection();
		findMapVMPM();
	}
	
	public static void setCollection()
	{
		Set<String> set = db.getCollectionNames();
		collectionPM.clear();
		collectionPMT.clear();
		collectionAll.clear();
		
		PMTcollectionIPs.clear();
		PMcollectionIPs.clear();
		allCollectionIPs.clear();

		for(String string:set)
		{
			if(string.contains(prefixPM))
			{
				if(string.contains(prefixPMT)) // PMT 
				{
					collectionPMT.add(db.getCollection(string));	
					collectionAll.add(db.getCollection(string));	
					
					PMTcollectionIPs.add(string.split(prefixPMT)[1]);
				}
				
				else	// PM 
				{
					collectionPM.add(db.getCollection(string));
					collectionAll.add(db.getCollection(string));
					
					PMcollectionIPs.add(string.split(prefixPM)[1]);
					allCollectionIPs.add(string.split(prefixPM)[1]);
				}
			}
		}		
	}

/*	public static void mapUpdate()
	{
		findMapVMPM();

		Iterator<String> it = mapTableVMPM.keySet().iterator();
		
		BasicDBObject document = new BasicDBObject();
		mapCollection.remove(document);
		
		while(it.hasNext())
		{
			String key = it.next();	
			
//			BasicDBObject query = new BasicDBObject("VM", key);
//			BasicDBObject PMIP = new BasicDBObject("$set", new BasicDBObject("PM", mapTableVMPM.get(key)));

			BasicDBObject VMPM = new BasicDBObject("VM",key).append("PM",mapTableVMPM.get(key));			
//			mapCollection.update(query,PMIP, true, false);	
			mapCollection.insert(VMPM);
		}						
	}	
*/	
	public static void findMapVMPM() 
	{				
		mapTableVMPM.clear();
		DBCollection pmtColl;
		
		for(int i = 0; collectionPMT.size() > i; i++)
		{	
			pmtColl = collectionPMT.get(i);
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();	
			DBCursor cursor;
			String VMIP = "";
			
			query.put("ip", 1); //Query : db["PMT10.1.0.41"].find({},{'ip':1})
			
			cursor = pmtColl.find(field,query);			
			while(cursor.hasNext())
			{			
				BasicDBObject result = (BasicDBObject)cursor.next();
				VMIP = result.getString("ip");
				allCollectionIPs.add(VMIP);

				mapTableVMPM.put(VMIP, PMTcollectionIPs.get(i));
			}
		}
	}

	public static String getMappedPMIP(String vmIP)
	{
//		db.pmvmMapTable.find({VM:"10.8.8.22"})

		BasicDBObject query=new BasicDBObject();
		query.put("VM",vmIP);
		DBCursor cursor = mapCollection.find(query);
		String pmIP = "";
		
		while(cursor.hasNext())
		{			
			BasicDBObject result = (BasicDBObject)cursor.next();
			pmIP = result.getString("PM");
		}
		
		System.out.println("Map >> " + vmIP + " " + pmIP);
		return pmIP;		
	}
	
	
	public static ArrayList<String> getMappedVMIP(String pmIP)
	{
		ArrayList<String> vmIPlist = new ArrayList<String>();
		
		BasicDBObject query=new BasicDBObject();
		query.put("PM",pmIP);
		DBCursor cursor = mapCollection.find(query);
		String vmIP = "";
		
		while(cursor.hasNext())
		{			
			BasicDBObject result = (BasicDBObject)cursor.next();
			vmIP = result.getString("VM");
			vmIPlist.add(vmIP);
			
			System.out.println("Map List >> " + pmIP + " " + vmIP);
		}
		
		return vmIPlist;		
	}
	
	public static ArrayList<String> getGroup(String ip) // We don't know whether the parameter 'ip' is PM's ip or VM's ip
	{
		ArrayList<String> historyGroup = new ArrayList<String>();
		
		String pm = "";
		ArrayList<String> VMs = new ArrayList<String>();

		pm = getMappedPMIP(ip);
		
		if(pm.equals("")) // this ip is not VM's ip ==> PM's IP
		{
			VMs = getMappedVMIP(ip);
			historyGroup.add(ip);
		}
		
		else
		{
			VMs = getMappedVMIP(pm);
			historyGroup.add(pm);
		}		
		
		historyGroup.addAll(VMs);
		
		return historyGroup;  // The first element is PM's ip and the others are VMs' ips
	}	
	
	public static DBCollection getCollectionPM(int index)
	{
		return collectionPM.get(index);
	}
	
	public static DBCollection getCollectionPMT(int index)
	{
		return collectionPMT.get(index);
	}
	
	public static DBCollection getCollectionAll(int index)
	{
		return collectionAll.get(index);
	}
	
	public static ArrayList<String> getPMTcollectionIPs()
	{
		return PMTcollectionIPs;
	}
	
	public static ArrayList<String> getPMcollectionIPs()
	{
		return PMcollectionIPs;
	}
	
	public static ArrayList<String> getAllCollectionIPs()
	{
		return allCollectionIPs;
	}
	
	public static int numPMcollection()
	{
		int num = 0;
		
		num = collectionPM.size();
		return num;
	}
	
	public static int numPMTcollection()
	{
		int num = 0;
		
		num = collectionPMT.size();
		return num;
	}	
	
	public static int numVMs()
	{
		int num = 0;
		
		num = mapTableVMPM.size();
		return num;
	}
}
