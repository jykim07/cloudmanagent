package configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
/**
 * @author Ke Wang
 */
public class Configuration {
    private static boolean debug = false;
    private static String remote = null;
    private static String backup = null;
    private static int port;
	private static int servicePort;
	private static int SOCKETPORT;//The port that socket servers are listening
	
	// Utilization
	private static int utilInterval_percent;
	private static int utilInterval_bandwidth; // MBps
	
	// System
	private static String virtualBrdgIP = null;
    
	// Hypervisor
	private static String hyperName = "qemu:///system";
	
	// Response time
	private static long samplingTime; //ms
	private static long errorTimeout; //ms
	
	public static void setConfiguration()
	{
		try
		{
			File readFile = new File("./Config.txt");
			BufferedReader inFile = new BufferedReader(new FileReader(readFile));
			String sLine = null;
			
			while((sLine = inFile.readLine()) != null)
			{
				String parameter = sLine.split("=")[0];
				String value = sLine.split("=")[1];
				
				if(parameter.equals("debug"))
					debug = Boolean.valueOf(value);
				
				else if(parameter.equals("remote IP address"))
					remote = value;

				
				else if(parameter.equals("backup IP address"))
					backup = value;
				
				else if(parameter.equals("port"))
					port = Integer.parseInt(value);
				
				else if(parameter.equals("service port"))
					servicePort = Integer.parseInt(value);
				
				else if(parameter.equals("socket port"))
					SOCKETPORT = Integer.parseInt(value);
				
				else if(parameter.equals("interval of utilization (percent)"))
					utilInterval_percent = Integer.parseInt(value);
				
				else if(parameter.equals("interval of utilization (bandwidth)"))
					utilInterval_bandwidth = Integer.parseInt(value);
				
				else if(parameter.equals("virtual bridge IP address"))
					virtualBrdgIP = value;
				
				else if(parameter.equals("hypervisor"))
					hyperName = value;				
								
				else if(parameter.equals("sampling time"))
					samplingTime = Long.parseLong(value);
				
				else if(parameter.equals("error timeout"))
					errorTimeout = Long.parseLong(value);

				sLine = null;
			}
			inFile.close();
		}
		catch(Exception ex)
		{
	    	ex.printStackTrace();
		}
	}	
	
    public static int getSocketport(){
    	return SOCKETPORT;
    }
	
    public static int getServicePort(){
    	return servicePort;
    }	
    
    public static boolean getDebug(){
        return debug;
    }
	
    public void setDebug(boolean debug){
        Configuration.debug = debug;
    }
    
    public static String getRemoteserver(){
    	return remote;
    }
    
    public static String getBackupserver(){
    	return backup;
    }
	
	// System
	public static String getVirtualBrdgIP(){
		return virtualBrdgIP;
	}
	
	// update utilization
	public static int getUtilPercent(){
    	return utilInterval_percent;
    }
	
	public static int getUtilBandwidth(){
		return utilInterval_bandwidth;
	}
	
	// Hypervisor
	public static String getHyperName(){
		return hyperName;
	}	
	
	public static long getSamplingTime(){
    	return samplingTime;
    }
	
	public static long getErrorTimeout(){
		return errorTimeout;
	}
	
	
	
    /**
     * 
     * @return a list of IPs of the vms that runs on this physical machine
     */
/*    public static String[] getvms(){
        File file = new File("ip.list");
        BufferedReader br = null;
        List<String> list = new ArrayList<String>();
        try {
            br = new BufferedReader(new FileReader(file));
            String str = null;
            while( (str = br.readLine()) != null) {
                list.add(str);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list.toArray(new String[0]); 
    }
*/            
    public static int getPort(){
        return port;
    }
    public void setPort(int port){
        Configuration.port = port;
    }
 /*   
    public static String getvmScript(){
        return vmlist_script;
    }
*/
}
