package traffic_monitor;

import java.util.ArrayList;  
import java.util.HashMap;
import java.util.Iterator;
import org.jnetpcap.JBufferHandler;
import org.jnetpcap.Pcap;  
import org.jnetpcap.PcapHeader;
import org.jnetpcap.PcapIf;
import org.jnetpcap.PcapAddr;
import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.nio.JMemory;
import org.jnetpcap.packet.PcapPacket;  
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;

import java.util.List;
import java.io.IOException;

public class PcapTest {  
	private final ArrayList<Flow> flows = new ArrayList<Flow>();
	private Pcap pcap = null;
	private String agentIP = null;
	static private ArrayList<String> nicNames = new ArrayList<String>();
	static private String pmNICName = null;

	public PcapTest(){}
	
	PcapIf netInterface = null;
	
	public PcapTest(String agentIP) throws IOException 
	{
		this.agentIP = agentIP;
		netInterface = getPcapNetInterface();
	}	
	
	public PcapIf getPcapNetInterface() throws IOException 
	{ 
		List<PcapIf> allInterface = new ArrayList<PcapIf>();		
		
		StringBuilder errorBuffer = new StringBuilder();
		int r = Pcap.findAllDevs(allInterface, errorBuffer);
		
		if(r == Pcap.NOT_OK || allInterface.isEmpty())
		{
			System.out.println("Can't read list of devices, error is >> " + errorBuffer.toString());
			return null;
		}

	   for (PcapIf dev : allInterface) 
	   {			
			nicNames.add(dev.getName());
			
			if(dev.toString().contains(agentIP))
			{
				netInterface = dev;		
				pmNICName = dev.getName();
			}					
		}
		
		return netInterface;
	}
	
    public ArrayList<Flow> getTraffic(int count) throws IOException{  
        StringBuilder errbuf = new StringBuilder();                                   // For any error msgs  
        flows.clear();
    	
        int snaplen = 96;                                                             // Capture only packets headers 
        int flags = Pcap.MODE_PROMISCUOUS;
        int timeout = 1000;                                                           // 2 seconds in millis  
		
		if(netInterface == null){
			return null;
		}

		System.out.println("======= NIC : [" + netInterface.getName() + "] ==========");

        pcap = Pcap.openLive(netInterface.getName(), snaplen, flags, timeout, errbuf);  
        if (pcap == null) {  
            System.err.printf("Error while opening device for capture: "  
                + errbuf.toString());  
            return null;  
        }
        
        JBufferHandler<String> jBufferHandler2 = new JBufferHandler<String>() {
        	private final PcapPacket packet = new PcapPacket(JMemory.POINTER);  
        	byte[] sIP = new byte[4];
        	byte[] dIP = new byte[4];
        	String sourceIP="";
        	String destIP="";
        	Tcp tcp = new Tcp();
        	Ip4 ip = new Ip4();
        	
            public void nextPacket(PcapHeader header, JBuffer buffer, String message) {
            	packet.peer(buffer);                                                  // Peer the data to our packet  
                packet.getCaptureHeader().peerTo(header,0);                           // Now peer the Pcap provided header  
                packet.scan(Ethernet.ID);                                             // Assuming that first header in packet is Ethernet 
                
                if (packet.hasHeader(ip) && packet.hasHeader(tcp)) { 
                	sIP = packet.getHeader(ip).source();
     		   		sourceIP = org.jnetpcap.packet.format.FormatUtils.ip(sIP);
     		   		dIP = packet.getHeader(ip).destination();
     		   		destIP = org.jnetpcap.packet.format.FormatUtils.ip(dIP);
     		   		
     		   		long timestamp = header.timestampInMicros()/1000;                // Time resolution is 1 milliseconds 
     		   		Flow flow = new Flow(sourceIP,tcp.source(),destIP,tcp.destination(),timestamp);
     		   		flow.setLength(header.caplen());
     		   		flows.add(flow);
                }
            }
        };
  
        pcap.loop(count, jBufferHandler2,"");  
        pcap.close();  
        return flows;
    } 

	static public ArrayList<String> getNICNames()
	{
		return nicNames;
	}
	
	static public String getPMNIC()
	{
		return pmNICName;
	}
}  
