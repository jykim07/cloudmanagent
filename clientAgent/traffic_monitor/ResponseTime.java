package traffic_monitor;

import configuration.Configuration;
import datatype.*;

import java.util.ArrayList;
import java.util.HashSet;

public class ResponseTime{ 
	
	private ArrayList<Delay> delayArray = new ArrayList<Delay>();
	private ArrayList<ArrayList<Flow>> classifiedFlow = new ArrayList<ArrayList<Flow>>();
	
	private String myVMIP = "";
	
	private long samplingTime = Configuration.getSamplingTime(); // 200ms
	private long errorTimeout = Configuration.getErrorTimeout(); // 3000 ms

	public ArrayList<Delay> analyzeRT(ArrayList<Flow> flows, String myVM)
	{		
		for(int i=0;i<flows.size();i++)
		{
			Flow flow = flows.get(i);			
		}
		
		myVMIP = myVM;
		classifyFlow(flows);	
		estimateRT();
		
		return delayArray;
	}
	
	public void classifyFlow(ArrayList<Flow> flows)
	{
		while(flows.size() > 0)
		{
			ArrayList<Flow> tempFlowArray = new ArrayList<Flow>();
			
			if(flows.size() > 0)
			{	
				Flow flow = flows.get(0);

				Delay delay = new Delay(flow.getSrcIp(), flow.getDstIp(), 0);
				delayArray.add(delay);				

				tempFlowArray.add(flow);	

				for(int i=1;i<flows.size();i++)
				{
					Flow nextFlow = flows.get(i);
				
					if(flow.getSrcIp().equals(nextFlow.getSrcIp()) && flow.getDstIp().equals(nextFlow.getDstIp()) || flow.getSrcIp().equals(nextFlow.getDstIp()) && flow.getDstIp().equals(nextFlow.getSrcIp()))
					{
						tempFlowArray.add(nextFlow);
						flows.remove(i);
						i--;
					}
				}
			}
			
			flows.remove(0);
			classifiedFlow.add(tempFlowArray);
		}				
	}
	
	public void estimateRT()
	{
		for(int i=0;i<classifiedFlow.size();i++)
		{
			ArrayList<Flow> flowArray = classifiedFlow.get(i);
			Delay delay = delayArray.get(i);
			
			while(flowArray.size() > 1)
			{
				int flowIndex = 0;				
				Flow flow = flowArray.get(flowIndex);
				
				int nextFlowIndex = flowIndex+1;
				Flow nextFlow = flowArray.get(nextFlowIndex);
				
				boolean end = false;
				
				while(flow.getSrcIp().equals(nextFlow.getSrcIp()) && flow.getDstIp().equals(nextFlow.getDstIp()))
				{
					if(flowArray.size() < 2)
					{
						end = true;
						break;
					}
					
					if(nextFlow.getTime() - flow.getTime() > errorTimeout)
					{
						flowArray.remove(flowIndex);
						continue;
					}
						
					else
					{
						nextFlowIndex++;
						
						if(flowArray.size() == nextFlowIndex)
						{
							end = true;	
							break;
						}
						
						nextFlow = flowArray.get(nextFlowIndex);
					}
				}
				
				if(end == true)
					break;
		
				long rt = samplingTime * ((nextFlow.getTime() - flow.getTime()) / samplingTime + 1);
					
				delay.setDelay(rt);				
				
				flowArray.remove(nextFlowIndex);				
				flowArray.remove(flowIndex);	
			}	
		}
		
		for(int i=0;i<delayArray.size();i++)
		{
			Delay delay = delayArray.get(i);

			if(delay.getSrcIp().equals(delay.getDstIp()) || !delay.getSrcIp().equals(myVMIP))
			{
				// We only consider the response time we observe another VMs
				delayArray.remove(i);
				i--;
				continue;
			}
		}	

		System.out.println("\n================= Response Time ==============");
		for(int i=0;i<delayArray.size();i++)
		{
			Delay delay = delayArray.get(i);
			
			System.out.println("[Src] " + delay.getSrcIp() + " [Dstn] " + delay.getDstIp() + " [RT] " + delay.getTime() + " ms");
		}	
		System.out.println("==============================================\n");
	}
}