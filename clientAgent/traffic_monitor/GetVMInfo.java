package traffic_monitor;

import configuration.Configuration;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.libvirt.Connect;
import org.libvirt.ConnectAuth;
import org.libvirt.ConnectAuthDefault;
import org.libvirt.LibvirtException;
import org.libvirt.Domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetVMInfo {  


	private String hyperName = Configuration.getHyperName();
	
	private ArrayList<String> commandARP = new ArrayList<String>(); // ARP command to get IP address
	
	private static String IPV4_PATTERN = 
		 "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
	private static Pattern pattern = Pattern.compile(IPV4_PATTERN);
	private static HashMap<String, String> vmNameIP = new HashMap<String, String>();
	
	Connect conn = null;
	Domain vm = null;
	
	public GetVMInfo() 
	{
		try 
		{			
			conn = new Connect(hyperName, true);	
		}
		catch(LibvirtException le) {
		   le.printStackTrace();
		}
	}
	
	public void getVMlist()
	{
		try 
		{	
			vmNameIP.clear();
			for (int id : conn.listDomains())
			{
				vm = conn.domainLookupByID(id);	// get VM
				getIPfromARPtable(vm);			
			}
		}
		catch(LibvirtException le) {
		   le.printStackTrace();
		}
	}
	
	public void disconnectLibvirt()
	{
		try 
		{
			if(vm != null)
				vm.free();		
				
			if(conn != null)
				conn.close();	
		}
		catch(LibvirtException le) {
		   le.printStackTrace();
		}	
	}
	
	public void getIPfromARPtable(Domain vm)
	{
		String cmd1 = "arp "; // no entry
		String cmd2 = "arp -a "; // more than one entry

		commandARP.add(cmd1);
		commandARP.add(cmd2);
		
		try 
		{	
			BufferedReader reader = null;
			// send ARP command
			for(int cmd = 0; commandARP.size() > cmd; cmd++) 
			{
				Process p = Runtime.getRuntime().exec(commandARP.get(cmd) + vm.getName()); 	
				p.waitFor();
			
				// get the result from ARP command
				reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

				String result = reader.readLine();
				if(result == null)
					continue;

				else
				{
					String vmIP = extractIPv4(result);	

					if(vmIP.equals(""))
						continue;
	
					else
					{
						vmNameIP.put(vm.getName(), vmIP);
						break;
					}
				}
			}
			commandARP.clear();
			reader.close();
		}
		catch(Exception e) {
		   e.printStackTrace();
		}	
	}

	// Extract a pattern of IP address from the ARP result
	public String extractIPv4(String str)
	{
		Matcher matcher = pattern.matcher(str);
		
		String ip = "";
				
		if(matcher.find())
			ip = matcher.group();
			
		return ip;
	}
	
	public HashMap<String, String> getVMNameIP()
	{
		return vmNameIP;
	}
}  

