package traffic_monitor;

import configuration.Configuration;
import datatype.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ParseTraffic {
    boolean debug = Configuration.getDebug();
    private String[] iplist;
	private String pmIP;
    private ArrayList<String> owner;
    private UtilizInfo util_info;
    private UtilizInfo old_util;
    private VMInfo vminfo;
    private VMInfo old_vminfo;
    private ChangeInfo change;
    private Map<String, Integer> request_per_server;
    private int relation = -1;
	//0: distributed, 1: composite, -1: client
    private Map<Integer,String> relation_translation;
    private int service_port = -1;                           
	
	/*If the vm provide a service, service_port is the famous port 
                                                                                     number of that service, otherwise it's -1;*/
    private ArrayList<Flow> flows = new ArrayList<Flow>();
    private ArrayList<Flow> filteredflows = new ArrayList<Flow>();
	
	private ArrayList<Delay> delay = new ArrayList<Delay>();
	private HashMap<ArrayList<String>, Long> oldDelay = new HashMap<ArrayList<String>, Long>();
    
    public ParseTraffic(String pmIP, String localIP, String[] iplist){
    	this.owner = new ArrayList<String>();
    	this.util_info = new UtilizInfo(pmIP);
    	this.old_util = new UtilizInfo(pmIP);
    	this.old_util.cpu = -10000;
    	this.old_util.disk = -10000;
    	this.old_util.mem = -10000;
    	this.old_util.netin = -10000;
    	this.old_util.netout = -10000;
    	this.vminfo = new VMInfo(pmIP,localIP);
    	this.old_vminfo = new VMInfo(pmIP, localIP);
    	this.change = new ChangeInfo(pmIP, localIP);
    	this.request_per_server = new HashMap<String, Integer>();
    	this.relation_translation = new HashMap<Integer,String>();
    	this.relation_translation.put(0, "Distributed");
    	this.relation_translation.put(1, "Composite");
    	this.relation_translation.put(2, "Concurrent");
    	this.iplist = iplist;
    }
    
    public void setIPlist(String[] iplist){
    	this.iplist = iplist;
    }
	
    public void setOwnerlist(ArrayList<String> owner){
    	this.owner = owner;
    }
    
    public void setOwner(String owner){
    	this.vminfo.owner = owner;
    }
	
    public void filterRecord(final ArrayList<Flow> newflows){
    	filteredflows.clear();
        flows = newflows;
        for(int i=0;i<flows.size();i++){
        	boolean pass = false;
        	Flow flow = flows.get(i);
				
			if(flow.getDstIp().equals(this.vminfo.localhost) || flow.getSrcIp().equals(this.vminfo.localhost))
					pass = true;
						
			if(pass==true){
        		if(flow.getDstIp().equals(this.vminfo.localhost)){                               // This is an incoming message
        			filteredflows.add(flow);
        		}
        		else if(flow.getSrcIp().equals(this.vminfo.localhost)){                          // This is an outgoing message
        			flow.setIncoming(false);
        			filteredflows.add(flow);
        		}
        	}
			
			else
				continue;
        }
    }
    
    public void compute_service(){
    	this.service_port = Configuration.getServicePort();
    }
    
    public void analyze(){
    	compute_service();
    	
    	double total_in_request = 0;
    	double total_out_request = 0;
    	this.relation = -1;
    	
    	this.vminfo.neighbor.clear();
    	this.vminfo.clients.clear();
    	this.vminfo.servers.clear();
    	this.request_per_server.clear();
    	
    	for(int k=0;k<filteredflows.size();k++){
    		Flow flow = filteredflows.get(k);
			
    		if(flow.getIncoming()){
    			if(flow.getDstPort()==this.service_port){                // This flow is a request from a remote client
    				if(!vminfo.clients.contains(flow.getSrcIp()))
      					vminfo.clients.add(flow.getSrcIp());
    				total_in_request++;
    			}
    			else{                                                    // This flow is a reply from a remote server
    				if(!vminfo.servers.contains(flow.getSrcIp()))
    	   				vminfo.servers.add(flow.getSrcIp());
    			}
    		}
    		else{
    			if(flow.getSrcPort()==this.service_port){                // This flow is a reply to a remote client
    				if(!vminfo.clients.contains(flow.getDstIp()))
    	   				vminfo.clients.add(flow.getDstIp());
    			}
    			else{                                                    // This flow is a request to a remote server
    				if(!vminfo.servers.contains(flow.getDstIp()))
    	   				vminfo.servers.add(flow.getDstIp());
    				total_out_request++;
    			}
    		}
    	}
    	
    	if(total_in_request>0){
    		if(total_out_request>(total_in_request*1.5))
    			this.relation = 1;                                       // Composite
    		else
    			this.relation = 0;                                       // Distributed
    	}
		
    	this.vminfo.relation = this.relation;
    	System.out.println("******"+this.vminfo.localhost+"--client: "+this.vminfo.clients.size()+"--server: "+this.vminfo.servers.size());
    	
    	this.change = this.compareOldVM(old_vminfo);
    	if(this.change.changed==true){
    		System.out.println("relation: "+this.change.relation+" new clients: "+this.change.newclients.size()+" new servers: "+this.change.newservers.size()+" old ones: "+this.change.oldneighbors.size());
    		//the graph has changed since last monitoring, we have to update the information and send it to the remote server
    		this.old_vminfo.relation = this.vminfo.relation;
    		this.old_vminfo.clients.clear();
    		Iterator<String> it;
    		for(it=vminfo.clients.iterator();it.hasNext();){
    			String client = it.next();
    			this.old_vminfo.clients.add(client);
    		}
    		this.old_vminfo.servers.clear();
    		for(it=vminfo.servers.iterator();it.hasNext();){
    			String server = it.next();
    			this.old_vminfo.servers.add(server);
    		}
			
    		Sendtoserver(1);
    	}
    }
	
	public void analyzeResponseTime()
	{
		ResponseTime rt = new ResponseTime();
		delay = rt.analyzeRT(filteredflows, this.vminfo.localhost);
		
		if(delay.size() > 0)
			Sendtoserver(3);
	}
	
   
    public void Sendtoserver(int infoType){
    	/**
    	 * @param isXML indicates whether the information to be sent is an IP message or an XML message.
    	 * 
    	 * This function transmits two types of information. When the agent is set up, 
    	 * it will pass the VM IP list to the server. Second, for each VM,
    	 * when we discover a new connection, we will pass that information to the server 
    	 * in order that the server can generate an XML file to feed the D3 for display.
    	 */
    	try {
    		@SuppressWarnings("resource")
			DatagramSocket socket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket(new byte[0], 0, InetAddress.getByName(Configuration.getRemoteserver()), Configuration.getSocketport());
	        
			DatagramPacket backup_packet = new DatagramPacket(new byte[0], 0, InetAddress.getByName(Configuration.getBackupserver()), Configuration.getSocketport());

	    	//these output streams will send information to the server.
	    	ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        	ObjectOutputStream objectStream = new ObjectOutputStream(byteArrayStream);
        	
        	/*construct the information to be sent. If this is a initial set up message, we should set isXml to false. 
        	 * Otherwise, set it to be true.
        	 */
        	RequestType request = new RequestType(this.vminfo.pmIPaddr, this.vminfo.localhost);
        	request.SetIsXml(infoType);
        	if(infoType==1){
        		//infoType 1 means this is a relationship information package
        		request.SetChangeInfo(this.change);
        	}
			
			else if(infoType==0){
        		//infoType 0 means this is a heartbeat
        		for(int i=0;i<iplist.length;i++)
        			request.AddVmipList(iplist[i]);
        		request.setOwnerList(this.owner);
        	}
 
			else if(infoType==2){
        		//infoType 2 means this is a utilization information package
        		request.SetUtilizInfo(this.util_info);
        	}
			
        	else if(infoType==3){
        		//infoType 3 means this is a delay information package
				for(int i=0;i<delay.size();i++)
				{
					Delay oneDelay = delay.get(i);	
					
					ArrayList<String> srcDstn = new ArrayList<String>();
					srcDstn.add(oneDelay.getSrcIp());
					srcDstn.add(oneDelay.getDstIp());
					
					long rt = oneDelay.getTime();
					
					Iterator<ArrayList<String>> it = oldDelay.keySet().iterator();
					
					while(it.hasNext())
					{
						ArrayList<String> key = it.next();
						
						if(srcDstn.equals(key))
						{
							if(oldDelay.get(key).equals(rt)) // we only send changed response time
							{								
								delay.remove(i);
								i--; 
								break;
							}
						}
					}
				}
				
				for(int i=0;i<delay.size();i++)
				{					
					Delay oneDelay = delay.get(i);
					
					request.SetDelayInfo(oneDelay.getSrcIp(), oneDelay.getDstIp(), oneDelay.getTime());		
					
					ArrayList<String> srcDstn = new ArrayList<String>();
					srcDstn.add(oneDelay.getSrcIp());
					srcDstn.add(oneDelay.getDstIp());
					
					oldDelay.put(srcDstn,oneDelay.getTime());
				}
        	}
			
			else if(infoType==4){
        		//infoType 0 means this is a heartbeat
        		for(int i=0;i<iplist.length;i++)
        			request.AddVmipList(iplist[i]);
        	}
			
        	objectStream.writeObject(request);
        	//transform the data to bytes in order to use UDP
        	byte[] arr = byteArrayStream.toByteArray();
        	packet.setData(arr);
        	backup_packet.setData(arr);
        	
        	socket.send(packet);
        	socket.send(backup_packet);
			
			socket.close();			
			
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void SendUtil() throws UnknownHostException{
		GetUtilization getUtil = new GetUtilization();
    	
    	this.util_info.diff[0] = false;
    	this.util_info.diff[1] = false;
    	this.util_info.diff[2] = false;
    	this.util_info.diff[3] = false;
    	this.util_info.diff[4] = false;

    	BufferedReader in;
						
			this.util_info.cpu = getUtil.getCPUUsage();
			this.util_info.mem = getUtil.getMemoryUsage();
			this.util_info.disk = getUtil.getDiskUsage();
			
			getUtil.getNetworkUtil();
			this.util_info.netin = getUtil.getInboundTraffic();
			this.util_info.netout = getUtil.getOutboundTraffic();

			System.out.println("\n========== Utilization ==========");
			System.out.println("[CPU] " + this.util_info.cpu + " %");
			System.out.println("[RAM] " + this.util_info.mem + " %");
			System.out.println("[Disk] " + this.util_info.disk + " %");
			System.out.println("[RX] " + this.util_info.netin + " Bps");
			System.out.println("[TX] " + this.util_info.netout + " Bps\n");
			
			boolean diff = false;                                            //begin to compare to util info with last time
			if(utilscale100(this.util_info.cpu)!=utilscale100(this.old_util.cpu)){
				this.util_info.diff[0] = true;
				this.old_util.cpu = this.util_info.cpu;
				diff = true;
			}
			if(utilscale100(this.util_info.mem)!=utilscale100(this.old_util.mem)){
				this.util_info.diff[1] = true;
				this.old_util.mem = this.util_info.mem;
				diff = true;
			}
			if(utilscale100(this.util_info.disk)!=utilscale100(this.old_util.disk)){
				this.util_info.diff[2] = true;
				this.old_util.disk = this.util_info.disk;
				diff = true;
			}
			if(utilscale1000(this.util_info.netin)!=utilscale1000(this.old_util.netin)){
				this.util_info.diff[3] = true;
				this.old_util.netin = this.util_info.netin;
				diff = true;
			}
			if(utilscale1000(this.util_info.netout)!=utilscale1000(this.old_util.netout)){
				this.util_info.diff[4] = true;
				this.old_util.netout = this.util_info.netout;
				diff = true;
			}
			if(diff==true)
				Sendtoserver(2);
		 
    }
    
    public boolean compareVminfo(VMInfo vm){
    	/**
    	 * This returns whether the given vm equals to the this.vminfo
    	 * @return True if the two are the same, False if they are different.
    	 */
    	if(vm.relation!=this.vminfo.relation)
    		return false;
    	Iterator<String> it;
    	
    	if(vm.clients.size()!=this.vminfo.clients.size())
    		return false;
    	if(vm.servers.size()!=this.vminfo.servers.size())
    		return false;
    	
    	for(it=this.vminfo.clients.iterator();it.hasNext();){
    		String client = it.next();
    		if(!vm.clients.contains(client))
    			return false;
    	}
    	for(it=this.vminfo.servers.iterator();it.hasNext();){
    		String server = it.next();
    		if(!vm.servers.contains(server))
    			return false;
    	}
    	
    	return true;
    }
    
    public ChangeInfo compareOldVM(VMInfo oldvm){
    	ChangeInfo change = new ChangeInfo(this.vminfo.pmIPaddr, this.vminfo.localhost);
    	if(oldvm.relation==this.vminfo.relation)
    		change.relation = this.vminfo.relation;
    	else{
    		change.relation = this.vminfo.relation;
    		change.changed = true;
    		change.relationchange = true;
    	}
    	
    	Iterator<String> it;
    	for(it=this.vminfo.clients.iterator();it.hasNext();){                   //check for new clients
    		String client = it.next();
    		if(!oldvm.clients.contains(client)){
    			change.newclients.add(client);
    			change.changed = true;
    		}
    	}
    	for(it=this.vminfo.servers.iterator();it.hasNext();){                   //check for new servers
    		String server = it.next();
    		if(!oldvm.servers.contains(server)){
    			change.newservers.add(server);
    			change.changed = true;
    		}
    	}
    	for(it=oldvm.clients.iterator();it.hasNext();){                         //check for old clients
    		String client = it.next();
    		if(!this.vminfo.clients.contains(client)){
    			change.oldneighbors.add(client);
    			change.changed = true;
    		}
    	}
    	for(it=oldvm.servers.iterator();it.hasNext();){                         //check for old servers
    		String server = it.next();
    		if(!this.vminfo.servers.contains(server)){
    			change.oldneighbors.add(server);
    			change.changed = true;
    		}
    	}
    	return change;
    }

    public int utilscale1000(double u){
		int scale = Configuration.getUtilBandwidth();
		
    	if(u/scale<4)
    		return (int)u/scale;
    	else
    		return 4;
    }
    public int utilscale100(double u){
		int scale = Configuration.getUtilPercent();
		
    	if(u/scale<4)
    		return (int)u/scale;
    	else
    		return 4;
    }
    
}