package traffic_monitor;

import configuration.Configuration;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.lang.String.*;

import org.libvirt.LibvirtException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Main {

	static GetVMInfo vmInfo;
	
	public static void main(String[] args) throws UnknownHostException{
		Configuration.setConfiguration();
		String agent_ip = getIp(); 

		System.out.println("\n=============== PM ==============");
		System.out.println("[IP] " + agent_ip);		

		//get the IP address of this physical machine
		if(agent_ip==null){
			System.err.println("Cannot get the IP of this server.");
			return;
		}
		
		ParseTraffic handshake_with_server = new ParseTraffic(agent_ip, "agent",null);        //This engine is not for any specific vm, it's used for utilization and
                                                                                    //handshake with server
		
		String hyper = Configuration.getHyperName();
		
if(!hyper.equals("null"))
{
		Map<String, ParseTraffic> vm_engine = new HashMap<String,ParseTraffic>();
		vmInfo = new GetVMInfo();
				
		while(true){
			try{
				ArrayList<String> vmIPlist = new ArrayList<String>();
				ArrayList<String> vmname = new ArrayList<String>();
				
				ArrayList<String> allIPlist = new ArrayList<String>();

				vmInfo.getVMlist();
				
				// get names and IPs of VMs
				HashMap<String, String> vmNameIPMap = new HashMap<String, String>();
				vmNameIPMap = vmInfo.getVMNameIP();							
				
				Iterator<String> it = vmNameIPMap.keySet().iterator();
				
				System.out.println("\n============== VMs ==============");
				
				while(it.hasNext())
				{
					String name = it.next();
					
					if(vmNameIPMap.get(name) != null)
					{
						vmIPlist.add(vmNameIPMap.get(name));
						vmname.add(name);
						
						System.out.println("[VM] " + vmNameIPMap.get(name) + "(" + name + ")"); 
					}
				}
				
				String vm[] = new String[vmIPlist.size()];
				
				for(int i=0;i<vmIPlist.size();i++){
					vm[i] = vmIPlist.get(i);
				}		
				
				handshake_with_server.setIPlist(vm);
				handshake_with_server.setOwnerlist(vmname);
				handshake_with_server.Sendtoserver(0);				

				ArrayList<Flow> flows = new ArrayList<Flow>();
				PcapTest pcap = new PcapTest(agent_ip); // modified by JY
				
				handshake_with_server.SendUtil();                                   //send the utilization info of this cycle				
				
				flows = pcap.getTraffic(500);        //get the traffic to be flows		

				// Parsetraffic for this PM
				String pm[] = new String[1];
				pm[0] = agent_ip;
				
				allIPlist.add(pm[0]);
				allIPlist.addAll(vmIPlist);
				
				String pmvm[] = new String[allIPlist.size()];
				
				for(int i=0;i<allIPlist.size();i++){
					pmvm[i] = allIPlist.get(i);
				}		
				
				handshake_with_server.setIPlist(pmvm);				
				handshake_with_server.Sendtoserver(4); // current PM-VM list
				
				if(vm_engine.containsKey(pm[0]))
				{				
					ParseTraffic pt = vm_engine.get(pm[0]);
					pt.filterRecord(flows);
					pt.analyze();
					pt.analyzeResponseTime();
				}
				
				else
				{
					ParseTraffic pt = new ParseTraffic(agent_ip, pm[0],pm);
					vm_engine.put(pm[0], pt);
					pt.filterRecord(flows);
					pt.analyze();
					pt.analyzeResponseTime();
				}
				
				// Parsetraffic for the VMs
				for(int i=0;i<vm.length;i++){
					if(vm_engine.containsKey(vm[i])){
						ParseTraffic pt = vm_engine.get(vm[i]);
						pt.filterRecord(flows);
						pt.analyze();
						pt.analyzeResponseTime();
					}
					else{
						ParseTraffic pt = new ParseTraffic(agent_ip, vm[i],vm);               //This is a new vm, so create an analyse engine for it
						pt.setOwner(vmname.get(i));
						vm_engine.put(vm[i], pt);
						pt.filterRecord(flows);
						pt.analyze();
						pt.analyzeResponseTime();
					}
				}
			}
// 	
			catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
	}

	else
	{
		while(true){
			try{				
				String pm[] = new String[1];
				pm[0] = agent_ip;	
				
				handshake_with_server.setIPlist(pm);				
				handshake_with_server.Sendtoserver(4); // current PM-VM list
				
				ArrayList<Flow> flows = new ArrayList<Flow>();
				PcapTest pcap = new PcapTest(agent_ip); 
						
				handshake_with_server.SendUtil();                                   //send the utilization info of this cycle				
				
				flows = pcap.getTraffic(500);        //get the traffic to be flows		
							
				ParseTraffic pt = new ParseTraffic(agent_ip, pm[0],pm);
				pt.filterRecord(flows);
				pt.analyze();
				pt.analyzeResponseTime();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}		
	}
}

public static String getIp() throws UnknownHostException{
    try {
        InetAddress candidateAddress = null;
		String virtualBrdgIP = Configuration.getVirtualBrdgIP();
        // Iterate all NICs (network interface cards)...
        for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
            NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
            // Iterate all IP addresses assigned to each card...
            for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements();) {
                InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
                if (!inetAddr.isLoopbackAddress()) {

                    if (inetAddr.isSiteLocalAddress()) {
                        // Found non-loopback site-local address. Return it immediately...
						String resultIP = inetAddr.toString().split("/")[1];
						if(resultIP.equals(virtualBrdgIP))
							continue;
							
                        return resultIP;
                    }
                    else if (candidateAddress == null) {
                        // Found non-loopback address, but not necessarily site-local.
                        // Store it as a candidate to be returned if site-local address is not subsequently found...
                        candidateAddress = inetAddr;
                        // Note that we don't repeatedly assign non-loopback non-site-local addresses as candidates,
                        // only the first. For subsequent iterations, candidate will be non-null.												
                    }
                }
            }
        }
        if (candidateAddress != null) {
            // We did not find a site-local address, but we found some other non-loopback address.
            // Server might have a non-site-local address assigned to its NIC (or it might be running
            // IPv6 which deprecates the "site-local" concept).
            // Return this non-loopback candidate address...
            return candidateAddress.toString().split("/")[1];
        }
        // At this point, we did not find a non-loopback address.
        // Fall back to returning whatever InetAddress.getLocalHost() returns...
        InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
        if (jdkSuppliedAddress == null) {
            throw new UnknownHostException("The JDK InetAddress.getLocalHost() method unexpectedly returned null.");
        }
        return jdkSuppliedAddress.toString().split("/")[1];
    }
    catch (Exception e) {
        UnknownHostException unknownHostException = new UnknownHostException("Failed to determine LAN address: " + e);
        unknownHostException.initCause(e);
        throw unknownHostException;
    }
	}	
}
