package traffic_monitor;

public class Flow {
	private String srcip;
	private String dstip;
	private int srcport;
	private int dstport;
	private long time;
	private int length;
	private int count;
	private boolean incoming = true;                             // Indicate this flow is incoming to the vm
	
	public Flow(String src,int srcport,String dst,int dstport){
		this.srcip = src;
		this.dstip = dst;
		this.srcport = srcport;
		this.dstport = dstport;
		this.time = -1;
		this.length = -1;
		this.count = 1;
		this.incoming = true;
	}
	public Flow(String src,int srcport,String dst,int dstport, long time){
		this.srcip = src;
		this.dstip = dst;
		this.srcport = srcport;
		this.dstport = dstport;
		this.time = time;
		this.length = -1;
		this.count = 1;
		this.incoming = true;
	}
	public void setLength(int len){
		this.length = len;
	}
	
	public int getLength(){
		return this.length;
	}
	public void setIncoming(boolean flag){
		this.incoming = flag;
	}
	public boolean getIncoming(){
		return this.incoming;
	}
	
	public int getCount(){
		return this.count;
	}
	public void setCount(int count){
		this.count = count;
	}
	public String getSrcIp(){
		return this.srcip;
	}
	public String getDstIp(){
		return this.dstip;
	}
	public int getSrcPort(){
		return this.srcport;
	}
	public int getDstPort(){
		return this.dstport;
	}
	public long getTime(){
		return this.time;
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj==null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final Flow other = (Flow) obj;
	    if(!other.getDstIp().equals(dstip))
	    	return false;
	    if(!other.getSrcIp().equals(srcip))
	    	return false;
	    if(other.getDstPort()!=dstport)
	    	return false;
	    if(other.getSrcPort()!=srcport)
	    	return false;
	    return true;
	}
}
