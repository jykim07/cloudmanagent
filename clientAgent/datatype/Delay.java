package datatype;

public class Delay{
	private String srcIP;
	private String dstIP;
	private long sumDelay = 0;
	private long count = 0;
	private long avgDelay = 0;
	
	public Delay(String srcIP, String dstIP, long RT)
	{
		this.srcIP = srcIP;
		this.dstIP = dstIP;
		avgDelay = RT;
	}

	public String getSrcIp()
	{
		return srcIP;
	}
	
	public String getDstIp()
	{
		return dstIP;
	}
	
	public long getTime()
	{
		return avgDelay;
	}
	
	public void setDelay(long RT)
	{
		sumDelay += RT;
		count++;
		
		avgDelay = sumDelay / count; 
	}	
}
