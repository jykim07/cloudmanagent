package datatype;

import java.io.Serializable;
import java.util.ArrayList;


public class RequestType implements Serializable{
	//infoType: 0 stands for heartbeat, 1 stands for relationship info, 2 stands for utilization info
	private static final long serialVersionUID = 1L;
	private int infoType = 0;
	private ChangeInfo change;
	private ArrayList<String> vmipList;
	private ArrayList<String> ownership;
	private UtilizInfo util_info = null;
	private ArrayList<String> delay_info = new ArrayList<String>(); // 
	
	public RequestType(String pmIPaddr, String localhost){
		this.change = new ChangeInfo(pmIPaddr, localhost);
		this.vmipList = new ArrayList<String>();
	}
	
	public void SetChangeInfo(ChangeInfo change){
		this.change = change;
	}
	
	public void SetUtilizInfo(UtilizInfo ui){
		this.util_info = ui;
	}	
	
	public void SetDelayInfo(String src, String dst, long time){	//
		delay_info.add(src);
		delay_info.add(dst);
		delay_info.add(Long.toString(time));		
	}	
	
	public void AddVmipList(String vmip){
		vmipList.add(vmip);
	}
	
	public void setOwnerList(ArrayList<String>nameList){
		this.ownership = nameList;
	}
	
	public ArrayList<String> getOwnerList(){
		return this.ownership;
	}
	public void SetIsXml(int type){
		this.infoType = type;
	}
	
	public ChangeInfo getChange(){
		return this.change;
	}
	
	public UtilizInfo getUtil(){
		return this.util_info;
	}
	
	public ArrayList<String> getDelay(){	//
		return this.delay_info;
	}
	
	public ArrayList<String> getiplist(){
		return this.vmipList;
	}
	
	public int getType(){
		return this.infoType;
	}
	
}
