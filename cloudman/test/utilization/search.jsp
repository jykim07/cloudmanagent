<style>
.axis path, .axis line {
    fill: none;
    stroke: black;
    shape-rendering: crispEdges;
}
.axis text {
    font-family: sans-serif;
    font-size: 11px;
}

div.tooltip {
  position: absolute;
  text-align: left;
  line-height:1.8;
  vertical-align: middle;
  width: 150px;
  height: 130px;
  padding: 10px;
  font: 13px sans-serif;
  font-weight: bold;
  background: steelblue;
  border: 0px;
  border-radius: 8px;
  pointer-events: none;
  float: left;
  color: white;
}
</style>
<body style="background:#EAEAEA;">

<div style="float:right;">
<form id="form1" name="form1" method="post" >
<!--form id="form2" name="form2" method="post" action="graph.jsp" target="right"-->
<input type="text" id="searchstr" name="searchstr" onKeyPress="if(event.keyCode==13){findgraph();}" class="textbox" size="20">
<!--input type="submit" value="Search"-->
<input type="button" value="Search" onclick="findgraph();" class="sbttn">
<!--input type="reset" value="Reset"-->
</div>

<script type="text/javascript" language="javascript">

  function findgraph() { 
  if ( document.getElementById("searchstr").value == "")
                alert("Please enter something to search for.");
  else 
    findgraphname(document.getElementById("searchstr").value);  
  }  
  
  
  function findgraphname(str) {  
  if (str.indexOf("10.1.") >= 0){
   // alert("No available graph for this node..");
    document.form1.target="main";
    document.form1.action="revisesearch3.jsp?ip="+str;
    document.form1.submit();
    //document.form1.target="left2";
    //document.form1.action="result.jsp";
    //document.form1.submit();
    //alert("No available graph for this node..");
  }
  else if (str=="database"||str=="web server"||str=="load balancer"){  
    document.form1.target="main";
    document.form1.action="workloadsearch3.jsp?ip="+str;//"./vm.jsp?vmip="+str;
    document.form1.submit();
    //document.form1.target="left2";
    //document.form1.action="result.jsp";
    //document.form1.submit();
  }
//  else if (str.indexOf("10.8.") >=0 || str.indexOf("10.214.") >= 0 || str.indexOf("10.10.") >= 0 || str.indexOf("10.37") >= 0 ){  
//    document.form1.target="main";
//    document.form1.action="./vm.jsp?vmip="+str;
//    document.form1.submit();
    //document.form1.target="left2";
    //document.form1.action="result.jsp";
    //document.form1.submit();
//  }
  else {
	alert("Please input the right IP range");
  }
  }
</script>

