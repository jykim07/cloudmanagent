package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;

public class dependencyLinks
{
	static dbConnection db;
	static groupPacks group;
	static String link = "";
	
	static ArrayList<ArrayList<String>> linkData = new ArrayList<ArrayList<String>>();
 //db["PMT10.1.0.22"].aggregate({$unwind:"$neighbor"},{$project:{_id:0,"vmIP":"$ip","neighborIP":"$neighbor.ip","type":"$neighbor.type","dependency":"$neighbor.dependency"}});
 
	public static void searchLinks(int index)
	{	
		BasicDBObject query = new BasicDBObject();		
		query.put("_id",0);
		query.put("vmIP","$ip");
		query.put("neighborIP","$neighbor.ip");
		query.put("type","$neighbor.type");
		query.put("dependency","$neighbor.dependency");

		BasicDBObject project = new BasicDBObject("$project",query);	
		BasicDBObject unwind1 = new BasicDBObject("$unwind","$neighbor");	
		
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

		pipeline.add(unwind1);	
		pipeline.add(project);				
		
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
															
		Cursor cursor = db.getCollectionVM(index).aggregate(pipeline,aggregationOptions);

		while(cursor.hasNext())
		{	
			ArrayList<String> tempLink = new ArrayList<String>();
			BasicDBObject result = (BasicDBObject)cursor.next();
			
			if(result.getString("type").equals("server"))
			{
				tempLink.add(result.getString("vmIP")); // source
				tempLink.add(result.getString("neighborIP")); // destination
			}
				
			else
			{
				tempLink.add(result.getString("neighborIP")); // source
				tempLink.add(result.getString("vmIP")); // destination
			}

			tempLink.add(result.getString("dependency"));
			linkData.add(tempLink);
		}
	}
	
	public static String getLinks(String clickedIP)
	{
		ArrayList<String> getNeighborsPM = new ArrayList<String>();	
		String pmIP = ""; 
		int index = 0;
		ArrayList<String> getOneLinkArray = new ArrayList<String>();	
		
		linkData.clear();
		
		link = "";
		link += "{\"links\":[";
		
		index=db.getPMIndex(clickedIP); 
		pmIP = db.getPMIP(index);
		
		getNeighborsPM = group.getNeighborsPM();
		
		int cntNeighborPM = 0;
		while(getNeighborsPM.size() > cntNeighborPM)
		{
			index = db.getPMIndex(getNeighborsPM.get(cntNeighborPM));
			searchLinks(index);
			
			cntNeighborPM++;
		}
		
		ArrayList<ArrayList<String>> hashLinkData = new ArrayList<ArrayList<String>>(new HashSet<ArrayList<String>>(linkData));

		int cnt = 0;
		while(hashLinkData.size() > cnt)
		{
			getOneLinkArray = hashLinkData.get(cnt);

			int inCnt = 0;

			link += "{";
			
			while(getOneLinkArray.size() > inCnt)
			{
				switch(inCnt)
				{
					case 0: link += "\"source\":";
						break;
					case 1: link += "\"target\":";
						break;		
					case 2: link += "\"type\":";
						break;

					default: link += "";
				}
				link += "\"" + getOneLinkArray.get(inCnt) + "\"";
				inCnt++;
				
				if(inCnt != getOneLinkArray.size())
					link += ",";
			}
			
			link += "}";
			cnt++;
			
			if(hashLinkData.size() != cnt)
				link += ",";
		}
		
		link += "]}";
		
		return link;
	}
}