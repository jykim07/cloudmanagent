/* Main class of Cloudman */

package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;
import java.net.UnknownHostException;

public class cloudmanGUI
{
// Set physical information about mongodb
	static String dbIP = "127.0.0.1"; // IP address
	static int dbPort = 27017; // Port number
	static String dbName = "jy"; // DB name
	static String collNameUtil = "PM"; // the prefix of PM collection
	static String collNameVM = "PMT"; // the prefix of VM collection

	public cloudmanGUI() throws UnknownHostException{
		dbConnection db = new dbConnection(dbIP, dbPort, dbName, collNameUtil, collNameVM);
		utilCircles util = new utilCircles();
		groupPacks group = new groupPacks();
		dependencyLinks link = new dependencyLinks();
		logAnalysis log = new logAnalysis();
		ActionServlet servlet = new ActionServlet();
	}
	
	public static String getCollNamePrefix()
	{
		return collNameUtil;
	}
}

