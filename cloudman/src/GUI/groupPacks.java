package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;

public class groupPacks
{
	static dbConnection db;
	
	static String group = "";
	static int vmSize = 100; // change
	
	static ArrayList<String> myNeighborsPM = new ArrayList<String>();
	
	public static ArrayList<String> searchChildren(int index)
	{		
		ArrayList<String> myVMs = new ArrayList<String>();	
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query = new BasicDBObject();	
		DBCursor cursor; 
		
		query.put("ip", 1); //Query : db.PM2.find({},{'ip':1})
		cursor = db.getCollectionVM(index).find(field,query);	
			
		while(cursor.hasNext())
		{	
			BasicDBObject result = (BasicDBObject)cursor.next();
			
			String vmIP = (String)result.get("ip");
			myVMs.add(vmIP);		
		}
		return myVMs;
	}
	
	public static ArrayList<String> searchNeighbors(int index)
	{		
		ArrayList<String> myNeighbors = new ArrayList<String>();

// db["PMT10.1.0.22"].aggregate({$project:{_id:0,"neighbor":"$neighbor"}},{$unwind:"$neighbor"});
		BasicDBObject query = new BasicDBObject();		
		query.put("_id",0);
		query.put("neighbor","$neighbor");
		BasicDBObject project = new BasicDBObject("$project",query);	

		BasicDBObject unwind = new BasicDBObject("$unwind","$neighbor");	
			
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		pipeline.add(project);				
		pipeline.add(unwind);
		
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
					
		Cursor cursor = db.getCollectionVM(index).aggregate(pipeline,aggregationOptions);
	
		System.out.println("%%% : " + db.getPMIP(index));	
		while(cursor.hasNext())
		{	
			System.out.println("**** : " + db.getPMIP(index));	
			BasicDBObject result = (BasicDBObject)cursor.next();		
			
			BasicDBObject neighbors = (BasicDBObject)result.get("neighbor");		
				
			String neighborIP = (String)neighbors.get("ip");
			myNeighbors.add(neighborIP);	
		}
		ArrayList<String> newNeighbors = new ArrayList<String>(new HashSet<String>(myNeighbors));
		
		return newNeighbors;
	}
	
	public static ArrayList<ArrayList<String>> formGroup(int index)
	{		
		int PMCnt = 0;
		int myVMCnt = 0;
		int myNeighborCnt = 0;

		ArrayList<String> myVMs = new ArrayList<String>();		
		ArrayList<String> myNeighbors = new ArrayList<String>();	
		
		ArrayList<String> targetVMs = new ArrayList<String>();		
		ArrayList<String> targetNeighbors = new ArrayList<String>();	
		
		ArrayList<ArrayList<String>> PMVM = new ArrayList<ArrayList<String>>();
				
		myVMs = searchChildren(index);
		myNeighbors = searchNeighbors(index);		
		
		Set<String> newSet = new HashSet<String>(myVMs);
		newSet.addAll(myNeighbors);		
		ArrayList<String> merged = new ArrayList<String>(newSet); // myVMs+myNeighbors
		
		System.out.println("==> myVMs : " + index);	
		int cnt=0;
		while(myVMs.size()>cnt)
		{
			System.out.println(myVMs.get(cnt));
			cnt++;
		}

		System.out.println("==> myNeighbors : " + index);		
		cnt=0;
		while(myNeighbors.size()>cnt)
		{			
			System.out.println(myNeighbors.get(cnt));
			cnt++;
		}
		
		System.out.println("==> merged My : " + index);		
		cnt=0;
		while(merged.size()>cnt)
		{			
			System.out.println(merged.get(cnt));
			cnt++;
		}
		
		while(db.getNumCollection() > PMCnt)
		{
			int myCnt = 0;
			int targetCnt = 0;
			
			targetVMs = searchChildren(PMCnt);
			targetNeighbors = searchNeighbors(PMCnt);	
			
			Set<String> newSet2 = new HashSet<String>(targetVMs);
			newSet2.addAll(targetNeighbors);
			ArrayList<String> mergedTarget = new ArrayList<String>(newSet2); // targetVMs+targetNeighbors

			System.out.println("==> merged Neighbors : " + PMCnt);		
			cnt=0;
			while(mergedTarget.size()>cnt)
			{			
				System.out.println(mergedTarget.get(cnt));
				cnt++;
			}		
			while(merged.size() > myCnt)
			{
				targetCnt=0;
				while(mergedTarget.size() > targetCnt)
				{
					if(merged.get(myCnt).equals(mergedTarget.get(targetCnt)))
					{
						ArrayList<String> tempPMVM = new ArrayList<String>();
						
						System.out.println("*** found : " + db.getPMIP(PMCnt));
						tempPMVM.add(db.getPMIP(PMCnt));
						for(int targetVMCnt=0; targetVMs.size() > targetVMCnt; targetVMCnt++)
						{
							tempPMVM.add(targetVMs.get(targetVMCnt));						
							System.out.println("=== " + targetVMs.get(targetVMCnt));
						}
						PMVM.add(tempPMVM);
						
						break;
					}
					targetCnt++;
				}
				myCnt++;
			}	

			System.out.println("==> targetVMs : " + PMCnt);	
			
			cnt=0;
			while(targetVMs.size()>cnt)
			{
				System.out.println(targetVMs.get(cnt));
				cnt++;
			}

			System.out.println("==> targetNeighbors : " + PMCnt);		
			cnt=0;
			while(targetNeighbors.size()>cnt)
			{			
				System.out.println(targetNeighbors.get(cnt));
				cnt++;
			}

			PMCnt++;				
		}

		Set<ArrayList<String>> newHash = new HashSet<ArrayList<String>>(PMVM);	
		ArrayList<ArrayList<String>> mapPMVM = new ArrayList<ArrayList<String>>(newHash);				

		return mapPMVM;
	}
	
	public static String getGroup(String clickedIP)
	{
		String pmIP = "";
		int index = 0;
		ArrayList<ArrayList<String>> mapPMVM = new ArrayList<ArrayList<String>>();
		
		index=db.getPMIndex(clickedIP);
		pmIP = db.getPMIP(index);
		mapPMVM = formGroup(index);
		System.out.println("==> Merged PMVMs : " + index);

		group = "";
		
		if(mapPMVM.size()==0) // PM has no VM.
			group += "{\"name\" : " + "\"" + pmIP + "\", \"children\" : []}";
		
		else if(mapPMVM.size()==1) // PM has no neighbor.
		{	
			group += "{\"name\" : " + "\"" + pmIP + "\", \"children\" : ["; 
			
			ArrayList<String> getOneVMArray = new ArrayList<String>();	
			getOneVMArray = mapPMVM.get(0);

			int cnt=1;
			while(getOneVMArray.size() > cnt)
			{
				group += "{\"name\" : " + "\"";
				group += getOneVMArray.get(cnt);
				group += "\", \"size\" :";
				group += vmSize;
				group += "}";
				
				cnt++;
				if(cnt != getOneVMArray.size())
					group += ",";
			}
			group += "]}";
		}
		
		else // PM has more than one neighbor.
		{		
			group += "{\"name\" : " + "\"" + "group" + "\", \"children\" : ["; 
			myNeighborsPM.clear();
			
			ArrayList<String> getOneVMArray = new ArrayList<String>();	
			{
				int cntVM = 0;
				
				while(mapPMVM.size() > cntVM)
				{
					String neighborPM = "";
					
					getOneVMArray = mapPMVM.get(cntVM);
					neighborPM = getOneVMArray.get(0);
					
					group += "{\"name\" : " + "\"" + neighborPM + "\", \"children\" : [";
					myNeighborsPM.add(neighborPM); // to show all the dependencies in the same group

					int cnt=1;
					while(getOneVMArray.size() > cnt)
					{
						group += "{\"name\" : " + "\"";
						group += getOneVMArray.get(cnt);
						group += "\", \"size\" :";
						group += vmSize;
						group += "}";
						
						cnt++;
						if(cnt != getOneVMArray.size())
							group += ",";
					}
					group += "]}";
					
					cntVM++;
					if(cntVM != mapPMVM.size())
						group += ",";					
				}
			}
			group += "]}";
		}
	
		group += "";
		return group;
	}
	
	public static ArrayList<String> getNeighborsPM()
	{
		return myNeighborsPM;
	}
}
