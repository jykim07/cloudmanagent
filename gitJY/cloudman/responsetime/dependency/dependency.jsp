<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.lang.*,GUI.*" %>
<%@ page import="com.mongodb.*" %>
<%@ page import="json.simple.*" %>
<%@ page import="gson.*" %>

<html lang="en">

<head>
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/examples/justified-nav/justified-nav.css" rel="stylesheet">

<style>	

.Composite {
  stroke: navy;
  stroke-width: 3;
  opacity: 1;
}

.Concurrent {
  stroke: green;
  stroke-width: 3;
  opacity: 1;
}

.Distributed {
  stroke: orange;
  stroke-width: 3;
  opacity: 1;  
}

.Request {
  stroke: pink;
  stroke-width: 2;
  opacity: 0.5;
}

.util{
  font-size:13px;
  font-family: serif;
  stroke: black;
}

</style>

</head>

<body style="background:white;">
	
<form id="dataForm" >
	<input type="hidden" id="circlePM" value=""/>
	<input type="hidden" id="packPM" value=""/>
	<input type="hidden" id="linkPM" value=""/>
	<input type="hidden" id="changedPM" value=""/>
	<input type="hidden" id="changedLink" value=""/>
	<input type="hidden" id="deadPMs" value=""/>
	<input type="hidden" id="changedUtil" value=""/>
	<input type="hidden" id="changedRT" value=""/>
	<input type="hidden" id="pmRT" value=""/>	
	<input type="hidden" id="vmRT" value=""/>	
	<input type="hidden" id="historyRT" value=""/>	
	<input type="hidden" id="historyUtil" value=""/>
</form>

<%
	String ip = request.getParameter("ip");
	ip = "\"" + ip + "\"" ;
%>

<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <link type="text/css" rel="stylesheet" href="http://mbostock.github.io/d3/talk/20111018/style.css"/>


<script>



var pageWidth = window.innerWidth,
    pageHeight = window.innerHeight;
    if (typeof pageWidth != "number"){
        if (document.compatMode == "CSS1Compat"){
            pageWidth = document.documentElement.clientWidth;
            pageHeight = document.documentElement.clientHeight;
        } else {
            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;
        }
		
	}
	
var WIDTH = pageWidth/1.2, 
	HEIGHT = pageHeight/1.3, 
	MARGINS = {top: 20, right: 100, bottom: 30, left: 50};

var indexIP=<%=ip%>;
var data = ajaxHistoryRT(indexIP);
console.log(data);
var numIP = 0;
data.forEach(function(d){
	d.TS = new Date(+d.TS*1000);	
	numIP = d.IP.length;
});	
console.log(numIP);
console.log(data);


var dataGroup = d3.nest()
      .key(function(d) {return d.IP;})
      .entries(data);

console.log(JSON.stringify(dataGroup));

var currentTime = new Date();

var	xScale = d3.time.scale().range([MARGINS.left, WIDTH - MARGINS.right]).domain([d3.min(data, function(d) {return d.TS;}), currentTime]);
var yScale = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([0, d3.max(data, function(d) {return d.RT;})]);

/*
var xScale = d3.time.scale().range([0, WIDTH]);
var yScale = d3.scale.linear().range([HEIGHT, 0]);
	
var xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickSize(-HEIGHT, 0).tickPadding(6);
var yAxis = d3.svg.axis().scale(yScale).orient("right").tickSize(-WIDTH).tickPadding(6);
*/
var xAxis = d3.svg.axis().scale(xScale).tickSize(-HEIGHT).orient("bottom").tickSubdivide(true).tickPadding(10);
var yAxis = d3.svg.axis().scale(yScale).tickSize(-WIDTH).orient("left").tickSubdivide(true).tickPadding(10);

var lSpace = WIDTH/dataGroup.length;  

var color = d3.scale.category10();

	
// Draw lines
var lineGen = d3.svg.line()
	.x(function(d) {return xScale(d.TS);})
    .y(function(d) {return yScale(d.RT);})
    .interpolate("basis");

// Draw x axis and y axis
var svg = d3.select("body").append("svg")
	.attr("width", WIDTH + MARGINS.left + MARGINS.right)
    .attr("height", HEIGHT + MARGINS.top + MARGINS.bottom)
	.append("g")
		.attr("transform", "translate(" + MARGINS.left + "," + MARGINS.top + ")");
          
	



	
</script>
</body>
</div>
</div>

</html>

