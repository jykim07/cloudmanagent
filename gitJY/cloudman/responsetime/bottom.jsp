<!--%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>-->
<!--%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%-->
	
<html>
<head>
<title></title>

<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script> 

<!-- Navigation -->

<style type="text/css">

/*Title*/
#title {
  background-color: #990000;
  border-color: #0d3a62;
  color: white !important;
  font-family:"Adobe Fan Heiti Std B",Georgia,Serif;
  font-size: 35px;
  text-align: center;
  vertical-align: middle;
  margin: -10px 10px ;
  float: right;
  line-height:0.0001;
  padding: 0;
}
</style>
</head>

<body style="background:#990000;" onload="updateTime();">
<td>
<div style="float:left;" id="time">  </div>
        <script type="text/javascript"> 

        function updateTime(){
        var currentDate = new Date();
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        var seconds = currentTime.getSeconds();
    if (minutes < 10)
        minutes = "0" + minutes
    if (seconds < 10)
        seconds = "0" + seconds
        var suffix = "AM";
        if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
        }
        if (hours == 0) {
        hours = 12;
        }       
        var canvas = d3.select("body");

        var time = canvas.select("div")
                  .text(hours + ":" + minutes + ":" + seconds + " " + suffix + " " + month + "/" + day + "/" + year)
                  .style("color", "White")
                  .style("font-weight","bold")
				  .style("font-size","25px")
				  .style("float","left")
                  .style("font-family","Adobe Fan Heiti Std B")
				  .style("margin","0px 20px")
				  .style("line-height","2");
				  
        
        setTimeout(updateTime,1000);            
        }       
</script>

<!-- Brand and toggle get grouped for better mobile display -->
<div style="float:right;" id="title">
	<p><b><a href="main_RT.jsp" target="main" style="color:white; text-decoration:none; ">CloudMan</a></b></p>
</div>
</td>
</body>
</html>
