<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.lang.*,GUI.*" %>
<%@ page import="com.mongodb.*" %>
<%@ page import="json.simple.*" %>
<%@ page import="gson.*" %>

<HTML>
<HEAD>
<TITLE>Cloudman</TITLE>
</HEAD>
<meta charset="utf-8">

<style>

body {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.x.axis path {
  display: none;
}

.line {
  fill: none;
  stroke: #990000;
  stroke-width: 1.5px;
}

.data-point {
	stroke: #990000;
	stroke-width: 2;
	fill: #FFF;
}

.d3-tip {
  line-height: 1;
  font-weight: bold;
  padding: 15px;
  background: rgba(0, 0, 0, 0.8);
  font-size: 14px;
  color: #fff;
  border-radius: 2px;
}

/* Creates a small triangle extender for the tooltip */
.d3-tip:after {
  box-sizing: border-box;
  display: inline;
  font-size: 14px;
  width: 100%;
  line-height: 1;
  color: rgba(0, 0, 0, 0.8);
  content: "\25BC";
  position: absolute;
  text-align: center;
}

/* Style northward tooltips differently */
.d3-tip.n:after {
  margin: -1px 0 0 0;
  top: 100%;
  left: 0;
}


</style>
<body style="background:#EAEAEA;">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<form id="dataForm" >
	<input type="hidden" id="circlePM" value=""/>
	<input type="hidden" id="packPM" value=""/>
	<input type="hidden" id="linkPM" value=""/>
	<input type="hidden" id="changedPM" value=""/>
	<input type="hidden" id="changedLink" value=""/>
	<input type="hidden" id="deadPMs" value=""/>
	<input type="hidden" id="changedUtil" value=""/>
	<input type="hidden" id="changedRT" value=""/>	
	<input type="hidden" id="pmRT" value=""/>	
	<input type="hidden" id="vmRT" value=""/>	
</form>



<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="http://labratrevenge.com/d3-tip/javascripts/d3.tip.v0.6.3.js"></script>
<script>

function ajaxPMRT(){
	var ajaxResult="";    
	document.getElementById("pmRT").value="request";

	var msgCircle=$('#circlePM').val(); 
	var msgPack=$('#packPM').val(); 
	var msgLink=$('#linkPM').val(); 
	var msgChangedPM=$('#changedPM').val(); 
	var msgChangedLink=$('#changedLink').val(); 
	var msgDeadPM=$('#deadPMs').val(); 
	var msgUtil=$('#changedUtil').val(); 
	var msgRT=$('#changedRT').val(); 	
	var msgPMRT=$('#pmRT').val(); 
	var msgVMRT=$('#vmRT').val(); 

	$.ajax({
		'async': false,
		'url':'ActionServlet',
		'dataType':"json",
		'data':{circlePM:msgCircle,packPM:msgPack,linkPM:msgLink,changedPM:msgChangedPM,changedLink:msgChangedLink,deadPMs:msgDeadPM,changedUtil:msgUtil,changedRT:msgRT,pmRT:msgPMRT,vmRT:msgVMRT},
		'success':function(responseText){
			ajaxResult=responseText;
		}		
	});

	document.getElementById("pmRT").value=null; 
	return ajaxResult;
}

function ajaxVMRT(){
	var ajaxResult="";    
	document.getElementById("vmRT").value="request";

	var msgCircle=$('#circlePM').val(); 
	var msgPack=$('#packPM').val(); 
	var msgLink=$('#linkPM').val(); 
	var msgChangedPM=$('#changedPM').val(); 
	var msgChangedLink=$('#changedLink').val(); 
	var msgDeadPM=$('#deadPMs').val(); 
	var msgUtil=$('#changedUtil').val(); 
	var msgRT=$('#changedRT').val(); 	
	var msgPMRT=$('#pmRT').val(); 
	var msgVMRT=$('#vmRT').val(); 

	$.ajax({
		'async': false,
		'url':'ActionServlet',
		'dataType':"json",
		'data':{circlePM:msgCircle,packPM:msgPack,linkPM:msgLink,changedPM:msgChangedPM,changedLink:msgChangedLink,deadPMs:msgDeadPM,changedUtil:msgUtil,changedRT:msgRT,pmRT:msgPMRT,vmRT:msgVMRT},
		'success':function(responseText){
			ajaxResult=responseText;
		}	
	});

	document.getElementById("vmRT").value=null; 
	return ajaxResult;
}

var pageWidth = window.innerWidth,
    pageHeight = window.innerHeight;
    if (typeof pageWidth != "number"){
        if (document.compatMode == "CSS1Compat"){
            pageWidth = document.documentElement.clientWidth;
            pageHeight = document.documentElement.clientHeight;
        } else {
            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;
        }
  }

var margin = {top: 100, right: 20, bottom: 100, left: 50},
    width = pageWidth - margin.left - margin.right,
    height = pageHeight - margin.top - margin.bottom;

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
var timeInterval=7000;

function startHistory(){
<%
	cloudmanGUI gui = new cloudmanGUI();
	dbConnection db = new dbConnection();
	historyResponseTime historyRT = new historyResponseTime();
	
	mapTable map = new mapTable();
	
	Integer numPM = map.numPMcollection();
	Integer numVM = map.numVMs();
%>


var numPMs=<%=numPM+1%>;   //how many PM circles in the page
var numVMs=<%=numVM+1%>;
var intervalX_PM = width / numPMs;
var intervalX_VM = width / numVMs;
  
var parseDate = d3.time.format("%d-%b-%y").parse;

var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var line = d3.svg.line()
    .x(function(d,i) { return i*intervalX_PM; })
    .y(function(d) { return y(d.rt); });

	svg.selectAll(".line").remove();
   svg.selectAll(".data-point").remove();
   svg.selectAll("g").remove();	
//var data = ajaxPMRT();
var data = ajaxPMRT();

  x.domain(d3.extent(data, function(d) { return d.pm; }));
  y.domain(d3.extent(data, function(d) { return d.rt; }));

//  svg.append("g")
//      .attr("class", "x axis")
 //     .attr("transform", "translate(0," + height + ")")
  //    .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Response Time (ms)");

  svg.append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line)
	  .transition().duration(timeInterval-1000);
//}
//);

var maxDataPointsForDots = 4000;
var pointRadius = 4;

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
  
	return "<strong> PM - </strong> <span>" + d.pm + " </span> " + "<br>" + "<strong>  RT  -  </strong> <span >" + d.rt + " ms </span>" ;
  })
  
  svg.call(tip);

var circles = svg.selectAll('.data-point')
		.data(data)
		.enter()
			.append('svg:circle')
				.attr('class', 'data-point')
				.style('opacity', 1)
				.attr('cx', function(d, i) { return i*intervalX_PM })
				.attr('cy', function(d) { return y(d.rt) })
				.attr('r', function() { return (data.length <= maxDataPointsForDots) ? pointRadius :0 })
				.on('mouseover', tip.show)								
				.on('mouseout', tip.hide);

}
startHistory();
setInterval(startHistory, timeInterval);

</script>

</body>
</html>

