<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.lang.*,GUI.*" %>
<%@ page import="com.mongodb.*" %>
<%@ page import="json.simple.*" %>
<%@ page import="gson.*" %>


<HTML>
<HEAD>
<TITLE>Cloudman</TITLE>
</HEAD>
<meta charset="utf-8">
<style>

body {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.x.axis path {
  display: none;
}

.line {
  fill: none;
  stroke: steelblue;
  stroke-width: 1.5px;
}

</style>
<body>
<form id="dataForm" >
<form id="dataForm" >
	<input type="hidden" id="circlePM" value=""/>
	<input type="hidden" id="packPM" value=""/>
	<input type="hidden" id="linkPM" value=""/>
	<input type="hidden" id="changedPM" value=""/>
	<input type="hidden" id="changedLink" value=""/>
	<input type="hidden" id="deadPMs" value=""/>
	<input type="hidden" id="changedUtil" value=""/>
	<input type="hidden" id="changedRT" value=""/>
	<input type="hidden" id="pmRT" value=""/>	
	<input type="hidden" id="vmRT" value=""/>	
	<input type="hidden" id="historyRT" value=""/>	
</form>
</form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script>

function ajaxHistoryRT(indexIP){
	var ajaxResult="";     
	document.getElementById("historyRT").value=indexIP;                   
	var msgHistoryRT=$('#historyRT').val(); 

	$.ajax({
		'async': false,
		'url':'ActionServlet',
		'dataType':"json",
		'data':{historyRT:msgHistoryRT},
		'success':function(responseText){
			ajaxResult=responseText;
		}	
	});
	document.getElementById("historyRT").value=null;      
	return ajaxResult;
}

var margin = {top: 20, right: 80, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

//var parseDate = d3.time.format("%Y%m%d").parse;

var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.category10();

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var line = d3.svg.line()
    .interpolate("basis")
    .x(function(d) { return x(d.TS); })
    .y(function(d) { return y(d.RT); });

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var data = [{"date":"20111001","PM 10.1.0.13":"56.01","VM 10.10.0.30":"22.44","VM 10.10.0.37":"10.99"},{"date":"20111002","PM 10.1.0.13":"66.01","VM 10.10.0.30":"72.44","VM 10.10.0.37":"80.99"},{"date":"20111003","PM 10.1.0.13":"26.01","VM 10.10.0.30":"82.44","VM 10.10.0.37":"70.99"},{"date":"20111004","PM 10.1.0.13":"66.01","VM 10.10.0.30":"72.44","VM 10.10.0.37":"80.99"},{"date":"20111005","PM 10.1.0.13":"56.01","VM 10.10.0.30":"22.44","VM 10.10.0.37":"10.99"},{"date":"20111006","PM 10.1.0.13":"66.01","VM 10.10.0.30":"72.44","VM 10.10.0.37":"80.99"}]

  color.domain(d3.keys(data[0]).filter(function(key) { return key !== "TS"; }));

  data.forEach(function(d) {
    d.TS = parseDate(d.TS);
  });

  var ip = color.domain().map(function(IP) {
    return {
      IP: IP,
      values: data.map(function(d) {
        return {date: d.TS, RT: +d[IP]};
      })
    };
  });

  x.domain(d3.extent(data, function(d) { return d.TS; }));

  y.domain([
    d3.min(ip, function(c) { return d3.min(c.values, function(v) { return v.RT; }); }),
    d3.max(ip, function(c) { return d3.max(c.values, function(v) { return v.RT; }); })
  ]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Response Time (ms)");

  var city = svg.selectAll(".city")
      .data(cities)
    .enter().append("g")
      .attr("class", "city");

  city.append("path")
      .attr("class", "line")
      .attr("d", function(d) { return line(d.values); })
      .style("stroke", function(d) { return color(d.name); });

  city.append("text")
      .datum(function(d) { return {name: d.name, value: d.values[d.values.length - 1]}; })
      .attr("transform", function(d) { return "translate(" + x(d.value.date) + "," + y(d.value.temperature) + ")"; })
      .attr("x", 3)
      .attr("dy", ".35em")
      .text(function(d) { return d.name; });


</script>

</body>
</html>

