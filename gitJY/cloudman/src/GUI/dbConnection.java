package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;


public class dbConnection
{	
	static DB cloudmanDB;
	static ArrayList<DBCollection> collectionPMUtil = new ArrayList<DBCollection>();
	static ArrayList<DBCollection> collectionPMVM = new ArrayList<DBCollection>();	
	
	static ArrayList<String> PMTcollectionNames = new ArrayList<String>();
	
	static String prefixUtil = ""; // PM's name for utilization
	static String prefixVM = "";	// PM's name for VMs(neighbors)
	
	public dbConnection(String dbIP, int dbPort, String dbName, String collNameUtil, String collNameVM) throws MongoException
	{
		try {		
			Mongo mongoClient = new Mongo(dbIP, dbPort);
			cloudmanDB = mongoClient.getDB(dbName);			
			
			prefixUtil = collNameUtil;
			prefixVM = collNameVM;
			setCollection(prefixUtil,prefixVM); 
		
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}	
	}
	
	public dbConnection(){}
	
	public static DB getCurrentDB()
	{
		return cloudmanDB;
	}
	
	public static void setCollection(String prefixUtil, String prefixVM)
	{
		Set<String> set = cloudmanDB.getCollectionNames();
		collectionPMUtil.clear();
		
		for(String string:set)
		{
			if(string.contains(prefixUtil))
			{
				if(string.contains(prefixVM)) // PMT 
				{
					collectionPMVM.add(cloudmanDB.getCollection(string));
					PMTcollectionNames.add(string);
				}
				
				else	// PM 
					collectionPMUtil.add(cloudmanDB.getCollection(string));
			}
		}		
	}
	
	public static DBCollection getCollectionUtil(int index)
	{
		return collectionPMUtil.get(index);
	}
	
	public static DBCollection getCollectionVM(int index)
	{
		DBCollection temp = collectionPMUtil.get(index);
		
		int indexVM = 0;
		
		while(PMTcollectionNames.size() > indexVM)
		{
			if(PMTcollectionNames.get(indexVM).contains(getPMIP(index)))
			{
				temp = collectionPMVM.get(indexVM);
				break;
			}	
			else	
				indexVM++;
		}
		
		return temp;
	}
	
	public static String getPMIP(int index)
	{		
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query = new BasicDBObject();	
		DBCursor cursor;

		String PMIP = "";
		
		query.put("ip", 1); //Query : db.PM2.find({},{'ip':1})
		
		cursor = collectionPMUtil.get(index).find(field,query);			
		while(cursor.hasNext())
		{	
			BasicDBObject result = (BasicDBObject)cursor.next();
			PMIP = result.getString("ip");
		}
		
		return PMIP;
	}
	
	public static int getPMIndex(String pmIP)
	{		
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query = new BasicDBObject();	
		DBCursor cursor;

		int indexPM = 0;
		
		int numCol = 0;
		int flag = 0; 
		
		while(getNumCollection() > numCol)
		{
			query.put("ip", 1); //Query : db.PM2.find({},{'ip':1})
			cursor = collectionPMUtil.get(numCol).find(field,query);	

			while(cursor.hasNext())
			{	
				BasicDBObject result = (BasicDBObject)cursor.next();
				
				if(result.getString("ip").equals(pmIP))
				{
					indexPM = numCol;
					flag = 1; // found
					break;
				}
			}
			
			if(flag==1)
				break;
				
			else
				numCol++;
		}
		
		return indexPM;
	}

	public static int getNumCollection()
	{
		int numCollection = 0;
		
		numCollection = collectionPMUtil.size();
		
		return numCollection;
	}
}
