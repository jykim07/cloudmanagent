package GUI;

import java.util.*;
import java.lang.*;
import com.mongodb.*;
import java.net.UnknownHostException;

public class utilCircles
{
	static dbConnection db;
	
	static ArrayList<String> utilType = new ArrayList<String>();
	static ArrayList<String> utilValue = new ArrayList<String>();
	
	static String util = "";

	static BasicDBObject field = new BasicDBObject();	
	static BasicDBObject query = new BasicDBObject();
	
	public static void retrieveUtil()
	{
		int cnt = 0;
		setUtil();
		util = "[";	
		
		while(db.getNumCollection() > cnt)
		{			
			query.put("VM", 0);
			DBCursor cursor = db.getCollectionUtil(cnt).find(field,query);	
			
			while(cursor.hasNext())
			{	
				BasicDBObject obj = (BasicDBObject)cursor.next();
				String pm = obj.toString();		
				
				String temp[] = pm.split("\"util\"");
				util += temp[0]; 
				
				util += writeUtil(cnt);			
				util += "}";		
			}

			cnt++;		
			
			if(cnt!=db.getNumCollection())
				util += ",";
		}
		
		util += "]";
	}
	
	public static void setUtil()
	{
		utilType.clear();
		utilValue.clear();
		
		utilType.add("cpu");
		utilType.add("mem");
		utilType.add("disk");
		utilType.add("net_in");
		utilType.add("net_out");
	}
	
	public static String writeUtil(int index)
	{		
		String str = "";
		String strLV = "";
		
/* MongoDB Query - example
		{$project:{_id:0,utilResult:{$strcasecmp:["$util.cpu","10"]}}}
*/		

		BasicDBObject query = new BasicDBObject();		
		BasicDBObject field = new BasicDBObject();		
		query.put("_id",0); // Don't print ID field
		
		int numType=0;
		int numLevel=0;
		
		query.put("_id",0);
		query.put("cpu","$util.cpu");
		query.put("mem","$util.mem");		
		query.put("disk","$util.disk");
		query.put("net_in","$util.net_in");		
		query.put("net_out","$util.net_out");

		BasicDBObject project = new BasicDBObject("$project",query);	
			
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		pipeline.add(project);				
		
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
			
		Cursor cursor = db.getCollectionUtil(index).aggregate(pipeline,aggregationOptions);			
		
		BasicDBObject result = (BasicDBObject)cursor.next();
		
		while(utilType.size() > numType)
		{		
			String utilResult = result.getString(utilType.get(numType));
			
			str += "\"" + utilType.get(numType) + "\" : \"" + utilResult + "\"";
			
			if(numType+1 != utilType.size())
				str += " , ";
			
			numType++;		
		}
		
		return str;
	}
	
	// Return PM data including utilization
	public static String getUtil()
	{
		retrieveUtil();
		return util;
	}
}
