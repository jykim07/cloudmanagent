package monitor;

import com.mongodb.*;
import java.util.*;
import java.text.DecimalFormat;

public class setHistory
{		
	static Mongo mongo;
	static DB historyDB;	
	static DBCollection historyCollection;

	static setMapTable map;
	
	static ArrayList<String> IPs = new ArrayList<String>();
	static ArrayList<String> historyTypes = new ArrayList<String>();
	
	static int hourlyStartTime = 0;
	static int hourlyEndTime = 0;
	
	static int dailyStartTime = 0;
	static int dailyEndTime = 0;
	
	static int weeklyStartTime = 0;
	static int weeklyEndTime = 0;	
	
	static int monthlyStartTime = 0;
	static int monthlyEndTime = 0;

	static int yearlyStartTime = 0;
	static int yearlyEndTime = 0;
	
	static String lastTimeMin = null;
	static String lastTimeHour = null; 
	static String lastTimeWeek = null;
	static String lastTimeMonth = null;
	static String lastTimeYear = null;
	
	public setHistory(String dbIP, int dbPort, String dbName, String collName) throws MongoException 
	{	
		try {
				mongo = new Mongo(dbIP, dbPort);
				historyDB = mongo.getDB(dbName);
				historyCollection = historyDB.getCollection(collName);
			
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public setHistory(){}
	
	public void initialize()
	{
		ArrayList<DBCollection> allCollections = map.getAllCollections();
		IPs = map.getAllIPs();
		
		for(int i=0; allCollections.size() > i; i++)
		{
			BasicDBObject query = new BasicDBObject();		
			BasicDBObject field = new BasicDBObject();		
			
			query.put("_id",0);
			query.put("ip","$ip");
			query.put("cpu","$util.cpu");
			query.put("mem","$util.mem");		
			query.put("disk","$util.disk");
			query.put("net_in","$util.net_in");		
			query.put("net_out","$util.net_out");
			query.put("RT","$response_time");

			BasicDBObject project = new BasicDBObject("$project",query);	
				
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
			
			pipeline.add(project);				
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
				
			Cursor cursor = allCollections.get(i).aggregate(pipeline,aggregationOptions);			
			
			BasicDBObject result = (BasicDBObject)cursor.next();
			
			String ip = result.getString("ip");
			if(ip==null) continue;
					
			String cpu = result.getString("cpu");
			if(cpu.equals("[ ]")) cpu="0";
			
			String mem = result.getString("mem");
			if(mem.equals("[ ]")) mem="0";
			
			String disk = result.getString("disk");
			if(disk.equals("[ ]")) disk="0";
			
			String netin = result.getString("net_in");
			if(netin.equals("[ ]")) netin="0";
			
			String netout = result.getString("net_out");
			if(netout.equals("[ ]")) netout="0";
			
			String rt = result.getString("RT");
			if(rt==null) rt="0";
			
			System.out.println(ip + " " + cpu + " " + mem + " " + disk + " " + netin + " " + netout + " " + rt);
			
			// initialize
			BasicDBObject queryUpdate = new BasicDBObject("IP",ip);
			DBCursor cursorHistory = historyCollection.find(queryUpdate);
			
			if(!cursorHistory.hasNext())
			{
				BasicDBObject newIP = new BasicDBObject("IP",ip)
					.append("LCPU", cpu)
					.append("LRAM", mem)
					.append("LDisk", disk)
					.append("LInbound", netin)
					.append("LOutbound", netout)
					.append("LRT", rt);

				historyCollection.update(queryUpdate,newIP, true, false);
			}
		}

		historyTypes.add("CPU");
		historyTypes.add("RAM");
		historyTypes.add("Disk");
		historyTypes.add("Inbound");
		historyTypes.add("Outbound");
		historyTypes.add("RT");
		
		hourlyUpdate(); // update hourly/daily/weekly/monthly/yearly history
		dailyUpdate();
		weeklyUpdate();
		monthlyUpdate();
		yearlyUpdate();
	}
	
	public void hourlyUpdate()
	{
		hourlyStartTime = (int)(System.currentTimeMillis()/1000);

//		hourlyEndTime = hourlyStartTime + 60*60*2; // End Time = Start Time + 2 hours

		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{					
			public void run()
			{
				// update 
//				if((int)(System.currentTimeMillis()/1000) >= hourlyEndTime)
//				{				
					hourlyStartTime += 60*60;
//					hourlyEndTime = hourlyStartTime + 60*60*2;
		
					updateHour();	
					initializeMin();					
//				}								
			}
		}, 1000L*60*60, 1000L*60*60); // hourly
	}
	
	public void updateHour()
	{
		for(int j=0; historyTypes.size() > j; j++)
		{								//db.history.aggregate({$unwind:"$MIN"},{$match:{"MIN.TS":{"$lte":1442223981}}},{$project:{"IP":"$IP","RT":"$MIN.RT"}})
			String type = historyTypes.get(j);
			
			BasicDBObject matchQuery = new BasicDBObject("MIN.TS",new 	BasicDBObject("$lt",hourlyStartTime)); 
			BasicDBObject match = new BasicDBObject("$match",matchQuery);
			
			BasicDBObject query = new BasicDBObject();		
			query.put("IP","$IP");
			query.put(type,"$MIN."+type);
			
			BasicDBObject project = new BasicDBObject("$project", query);		
			
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
			
			pipeline.add(match);
			pipeline.add(project);	
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
																
			Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);

			while(cursor.hasNext())
			{
				double avg = 0;
				String value = "";
				
				BasicDBObject result = (BasicDBObject)cursor.next();	
				
				String ip = (String)result.get("IP");
				List<String> list = new ArrayList<String>();
				
				list = (List<String>)result.get(type);
				
				for(int i=0; list.size() > i ; i++)
				{
					if(list.get(i) != null)
						avg += Double.parseDouble(list.get(i));			
				}
				
				if(list.size() != 0)
					avg = avg / list.size();
				
				value = decimalFormat(Double.toString(avg));
				
				insertHour(ip, type, value);
			}
		}
	}
	
	public static void insertHour(String ip, String type, String value)
	{
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", hourlyStartTime)
				.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("HOUR",newValue));
						
		historyCollection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));
			
		historyCollection.update(queryIP, updateLastValue, true, false); 
	}
	
	public static void initializeMin()
	{
		for(int i=0; IPs.size() > i; i++)
		{
			BasicDBObject query = new BasicDBObject("IP",IPs.get(i));
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> : removed" + IPs.get(i));
			// remove all the fields of MIN
			BasicDBObject min = new BasicDBObject("$unset", new BasicDBObject("MIN",""));			
			historyCollection.update(query, min);
		}
		
		for(int i=0; historyTypes.size() > i ; i++)
		{		
			String type = historyTypes.get(i);
			// put the first MIN using LRT
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();	
			DBCursor cursor;

			String lvalue = "";
			String ip = "";
			query.put("IP", 1);		
			query.put("L"+type, 1); //db.history.find({},{'IP':1,'LRT':1});
				
			cursor = historyCollection.find(field,query);	
				
			while(cursor.hasNext())
			{	
				BasicDBObject result = (BasicDBObject)cursor.next();
				lvalue = result.getString("L"+type);
				ip = result.getString("IP");
				
				BasicDBObject queryIP = new BasicDBObject("IP",ip);
				BasicDBObject newValue = new BasicDBObject("TS", dailyStartTime)
					.append(type,lvalue);
				BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));
							
				historyCollection.update(queryIP,updateValue,true,false);			
			}
		}
	}

	// daily
	public void dailyUpdate()
	{
		dailyStartTime = (int)(System.currentTimeMillis()/1000);
//		dailyEndTime = dailyStartTime + 60*60*24*2; // End Time = Start Time + 2 hours

		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{					
			public void run()
			{
				// update 
//				if((int)(System.currentTimeMillis()/1000) >= dailyEndTime)
//				{				
					dailyStartTime += 60*60*24;
//					dailyEndTime = dailyStartTime + 60*60*24*2;

					updateDay();		
					initializeHour();
//				}								
			}
		}, 1000L*60*60*24, 1000L*60*60*24); // daily
	}
	
	public void updateDay()
	{
	//db.history.aggregate({$unwind:"$MIN"},{$match:{"MIN.TS":{"$lte":1442223981}}},{$project:{"IP":"$IP","RT":"$MIN.RT"}})
		for(int j=0; historyTypes.size() > j ; j++)
		{		
			String type = historyTypes.get(j);
			BasicDBObject matchQuery = new BasicDBObject("HOUR.TS",new 	BasicDBObject("$lt",dailyStartTime)); 
			BasicDBObject match = new BasicDBObject("$match",matchQuery);
			
			BasicDBObject query = new BasicDBObject();		
			query.put("IP","$IP");
			query.put(type,"$HOUR."+type);
			BasicDBObject project = new BasicDBObject("$project", query);		
			
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
			
			pipeline.add(match);
			pipeline.add(project);	
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
																
			Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);

			while(cursor.hasNext())
			{
				double avg = 0;
				String value = "";
				
				BasicDBObject result = (BasicDBObject)cursor.next();	
				
				String ip = (String)result.get("IP");
				List<String> list = new ArrayList<String>();
				
				list = (List<String>)result.get(type);
				
				for(int i=0; list.size() > i ; i++)
				{
					if(list.get(i) != null)
						avg += Double.parseDouble(list.get(i));			
				}
				
				if(list.size() != 0)
					avg = avg / list.size();
					
				value = decimalFormat(Double.toString(avg));
				
				insertDayValue(ip, type, value);
			}
		}			
	}
	
	public static void insertDayValue(String ip, String type, String value)
	{
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", dailyStartTime)
				.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("DAY",newValue));
						
		historyCollection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));
			
		historyCollection.update(queryIP, updateLastValue, true, false); 			
	}	
	
	public static void initializeHour()
	{
		for(int i=0; IPs.size() > i; i++)
		{
			BasicDBObject query = new BasicDBObject("IP",IPs.get(i));
			
			// remove all the fields of HOUR
			BasicDBObject min = new BasicDBObject("$unset",new BasicDBObject("HOUR",""));			
			historyCollection.update(query, min);
		}
		
		for(int i=0; historyTypes.size() > i ; i++)
		{		
			String type = historyTypes.get(i);
		
			// put the first HOUR using LRT
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();	
			DBCursor cursor;

			String lvalue = "";
			String ip = "";
			query.put("IP", 1);		
			query.put("L"+type, 1); //db.history.find({},{'IP':1,'LRT':1});
				
			cursor = historyCollection.find(field,query);	
				
			while(cursor.hasNext())
			{	
				BasicDBObject result = (BasicDBObject)cursor.next();
				lvalue = result.getString("L"+type);
				ip = result.getString("IP");
				
				BasicDBObject queryIP = new BasicDBObject("IP",ip);
				BasicDBObject newValue = new BasicDBObject("TS", dailyStartTime)
					.append(type,lvalue);
				BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("HOUR",newValue));
							
				historyCollection.update(queryIP,updateValue,true,false);			
			}
		}
	}

	// weekly
	public void weeklyUpdate()
	{
		weeklyStartTime = (int)(System.currentTimeMillis()/1000);
//		weeklyEndTime = weeklyStartTime + 60*60*24*7*2; // End Time = Start Time + 2 hours

		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{					
			public void run()
			{
				// update 
//				if((int)(System.currentTimeMillis()/1000) >= weeklyEndTime)
//				{				
					weeklyStartTime += 60*60*24*7;
//					weeklyEndTime = weeklyStartTime + 60*60*24*7*2;

					updateWeek();		
//				}								
			}
		}, 1000L*60*60*24*7, 1000L*60*60*24*7); // weekly
	}
	
	public void updateWeek()
	{
		//db.history.aggregate({$unwind:"$MIN"},{$match:{"MIN.TS":{"$lte":1442223981}}},{$project:{"IP":"$IP","RT":"$MIN.RT"}})
		for(int j=0; historyTypes.size() > j ; j++)
		{		
			String type = historyTypes.get(j);
			
			BasicDBObject matchQuery = new BasicDBObject("DAY.TS",new 	BasicDBObject("$lt",weeklyStartTime)); 
			BasicDBObject match = new BasicDBObject("$match",matchQuery);
			
			BasicDBObject query = new BasicDBObject();		
			query.put("IP","$IP");
			query.put(type,"$DAY."+type);
			BasicDBObject project = new BasicDBObject("$project", query);		
			
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
			
			pipeline.add(match);
			pipeline.add(project);	
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
																
			Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);

			while(cursor.hasNext())
			{
				double avg = 0;
				String value = "";
				
				BasicDBObject result = (BasicDBObject)cursor.next();	
				
				String ip = (String)result.get("IP");
				List<String> list = new ArrayList<String>();
				
				list = (List<String>)result.get(type);
				
				for(int i=0; list.size() > i ; i++)
				{
					if(list.get(i) != null)
						avg += Double.parseDouble(list.get(i));			
				}
				
				if(list.size() != 0)
					avg = avg / list.size();
					
				value = decimalFormat(Double.toString(avg));
				
				insertWeekValue(ip, type, value);
			}	
		}			
	}
	
	public static void insertWeekValue(String ip, String type, String value)
	{
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", weeklyStartTime)
				.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("WEEK",newValue));
						
		historyCollection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));
			
		historyCollection.update(queryIP, updateLastValue, true, false); 
	}	
	
// monthly
	public void monthlyUpdate()
	{
		monthlyStartTime = (int)(System.currentTimeMillis()/1000);
//		monthlyEndTime = monthlyStartTime + 60*60*24*30*2; // End Time = Start Time + 2 hours

		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{					
			public void run()
			{
				// update 
//				if((int)(System.currentTimeMillis()/1000) >= monthlyEndTime)
//				{				
					monthlyStartTime += 60*60*24*30;
//					monthlyEndTime = monthlyStartTime + 60*60*24*30*2;

					updateMonth();	
					initializeDay();						
//				}								
			}
		}, 1000L*60*60*24*30, 1000L*60*60*24*30); // monthly
	}
	
	public void updateMonth()
	{
	//db.history.aggregate({$unwind:"$MIN"},{$match:{"MIN.TS":{"$lte":1442223981}}},{$project:{"IP":"$IP","RT":"$MIN.RT"}})
		for(int j=0; historyTypes.size() > j ; j++)
		{		
			String type = historyTypes.get(j);
			
			BasicDBObject matchQuery = new BasicDBObject("DAY.TS",new 	BasicDBObject("$lt",monthlyStartTime)); 
			BasicDBObject match = new BasicDBObject("$match",matchQuery);
			
			BasicDBObject query = new BasicDBObject();		
			query.put("IP","$IP");
			query.put(type,"$DAY."+type);
			BasicDBObject project = new BasicDBObject("$project", query);		
			
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
			
			pipeline.add(match);
			pipeline.add(project);	
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
																
			Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);

			while(cursor.hasNext())
			{
				double avg = 0;
				String value = "";
				
				BasicDBObject result = (BasicDBObject)cursor.next();	
				
				String ip = (String)result.get("IP");
				List<String> list = new ArrayList<String>();
				
				list = (List<String>)result.get(type);
				
				for(int i=0; list.size() > i ; i++)
				{
					if(list.get(i) != null)
						avg += Double.parseDouble(list.get(i));			
				}
				
				if(list.size() != 0)
					avg = avg / list.size();
					
				value = decimalFormat(Double.toString(avg));
				
				insertMonthValue(ip, type, value);
			}				
		}
	}
	
	public static void insertMonthValue(String ip, String type, String value)
	{
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", monthlyStartTime)
				.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MONTH",newValue));
						
		historyCollection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));
			
		historyCollection.update(queryIP, updateLastValue, true, false); 
	}	
	
	public static void initializeDay()
	{
		for(int i=0; IPs.size() > i; i++)
		{
			BasicDBObject query = new BasicDBObject("IP",IPs.get(i));
			
			// remove all the fields of HOUR
			BasicDBObject min = new BasicDBObject("$unset",new BasicDBObject("DAY",""));			
			historyCollection.update(query, min);
		}
		
		for(int i=0; historyTypes.size() > i ; i++)
		{		
			String type = historyTypes.get(i);
			
			// put the first HOUR using LRT
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();	
			DBCursor cursor;

			String lvalue = "";
			String ip = "";
			query.put("IP", 1);		
			query.put("L"+type, 1); //db.history.find({},{'IP':1,'LRT':1});
				
			cursor = historyCollection.find(field,query);	
				
			while(cursor.hasNext())
			{	
				BasicDBObject result = (BasicDBObject)cursor.next();
				lvalue = result.getString("L"+type);
				ip = result.getString("IP");
				
				BasicDBObject queryIP = new BasicDBObject("IP",ip);
				BasicDBObject newValue = new BasicDBObject("TS", monthlyStartTime)
					.append(type,lvalue);
				BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("DAY",newValue));
							
				historyCollection.update(queryIP,updateValue,true,false);			
			}
		}
	}	

// yearly
	public void yearlyUpdate()
	{
		yearlyStartTime = (int)(System.currentTimeMillis()/1000);
//		yearlyEndTime = yearlyStartTime + 60*60*24*30*12*2; // End Time = Start Time + 2 hours

		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{					
			public void run()
			{
				// update 
//				if((int)(System.currentTimeMillis()/1000) >= yearlyEndTime)
//				{				
					yearlyStartTime += 60*60*24*30*12;
//					yearlyEndTime = yearlyStartTime + 60*60*24*30*12*2;

					updateYear();		
					initializeMonth();
//				}								
			}
		}, 1000L*60*60*24*30*12, 1000L*60*60*24*30*12); // yearly
	}
	
	public void updateYear()
	{		//db.history.aggregate({$unwind:"$MIN"},{$match:{"MIN.TS":{"$lte":1442223981}}},{$project:{"IP":"$IP","RT":"$MIN.RT"}})
		
		for(int j=0; historyTypes.size() > j ; j++)
		{		
			String type = historyTypes.get(j);
		
			BasicDBObject matchQuery = new BasicDBObject("MONTH.TS",new 	BasicDBObject("$lt",yearlyStartTime)); 
			BasicDBObject match = new BasicDBObject("$match",matchQuery);
			
			BasicDBObject query = new BasicDBObject();		
			query.put("IP","$IP");
			query.put(type,"$MONTH."+type);
			BasicDBObject project = new BasicDBObject("$project", query);		
			
			List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
			
			pipeline.add(match);
			pipeline.add(project);	
			
			AggregationOptions aggregationOptions = AggregationOptions.builder()
				.batchSize(100)
				.outputMode(AggregationOptions.OutputMode.CURSOR)
				.build();
																
			Cursor cursor = historyCollection.aggregate(pipeline,aggregationOptions);

			while(cursor.hasNext())
			{
				double avg = 0;
				String value = "";
				
				BasicDBObject result = (BasicDBObject)cursor.next();	
				
				String ip = (String)result.get("IP");
				List<String> list = new ArrayList<String>();
				
				list = (List<String>)result.get(type);
				
				for(int i=0; list.size() > i ; i++)
				{
					if(list.get(i) != null)
						avg += Double.parseDouble(list.get(i));			
				}
				
				if(list.size() != 0)
					avg = avg / list.size();
					
				value = decimalFormat(Double.toString(avg));
				
				insertYearValue(ip, type, value);
			}	
		}			
	}
	
	public static void insertYearValue(String ip, String type, String value)
	{
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", yearlyStartTime)
				.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("YEAR",newValue));
						
		historyCollection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));
			
		historyCollection.update(queryIP, updateLastValue, true, false); 	
	}	
	
	public static void initializeMonth()
	{
		for(int i=0; IPs.size() > i; i++)
		{
			BasicDBObject query = new BasicDBObject("IP",IPs.get(i));
			
			// remove all the fields of MONTH
			BasicDBObject min = new BasicDBObject("$unset",new BasicDBObject("MONTH",""));			
			historyCollection.update(query, min);
		}
		
		for(int i=0; historyTypes.size() > i ; i++)
		{		
			String type = historyTypes.get(i);
		
			// put the first MONTH using LRT
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();	
			DBCursor cursor;

			String lvalue = "";
			String ip = "";
			query.put("IP", 1);		
			query.put("L"+type, 1); //db.history.find({},{'IP':1,'LRT':1});
				
			cursor = historyCollection.find(field,query);	
				
			while(cursor.hasNext())
			{	
				BasicDBObject result = (BasicDBObject)cursor.next();
				lvalue = result.getString("L"+type);
				ip = result.getString("IP");
				
				BasicDBObject queryIP = new BasicDBObject("IP",ip);
				BasicDBObject newValue = new BasicDBObject("TS", yearlyStartTime)
					.append(type,lvalue);
				BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MONTH",newValue));
							
				historyCollection.update(queryIP,updateValue,true,false);			
			}
		}
	}
	
	public static String decimalFormat(String value)
	{
		double doubleValue = Double.parseDouble(value);		
		DecimalFormat format = new DecimalFormat();
        format.applyLocalizedPattern("0.##");
		
		doubleValue = Double.parseDouble(format.format(doubleValue));
		value = Double.toString(doubleValue);
		return value;
	}
	
	public static void updateHistory(String ip, String type, String value)
	{			
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", (int)(System.currentTimeMillis()/1000))
				.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));
						
		historyCollection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));
			
		historyCollection.update(queryIP, updateLastValue, true, false); 
	}
}
