package monitor;

import configuration.*;

import java.io.*;  
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.Cursor;
import com.mongodb.*;

import datatype.*;


public class Socketserver extends Thread{  
	
//	private Map<String, Boolean> ipmap;
	private Map<Integer,String> relation_translation;//0: Distributed, 1: Composite, 2: Concurrent, -1: request
    //private Logger logger = Logger.getLogger(Socketserver.class.getName()); 
	
    private Mongo mongoClient;
    private DB db;
	
	updateResponseTime rtClass;
	setHistory history;
    
    @SuppressWarnings("deprecation")
	public Socketserver(Map<String, Boolean> map){
 //   	this.ipmap = map;
    	this.relation_translation = new HashMap<Integer,String>();
		this.relation_translation.put(-1, "request");
    	this.relation_translation.put(0, "distributed");
    	this.relation_translation.put(1, "composite");
    	this.relation_translation.put(2, "concurrent");
    	mongoClient = new MongoClient("localhost", Configuration.getDBPort() );
    	db = mongoClient.getDB(Configuration.getDBName());
		rtClass = new updateResponseTime(db);
    }
    
    public void run() {  
    	
    	DatagramSocket socket;
		
    	try {
			socket = new DatagramSocket(Configuration.getSocketport());
			byte[] in_buff = new byte[2048];
			DatagramPacket in_packet = new DatagramPacket(in_buff, in_buff.length);
			System.out.println("Socekter server has started!");
			
			HashMap<String, ArrayList<String>> pmVMmap= new HashMap<String, ArrayList<String>>();
	
			while (true) {  
				socket.receive(in_packet);
				
				System.out.println("receiving a packet");
				ByteArrayInputStream in_byteArrayStream = new ByteArrayInputStream(in_buff);
				ObjectInputStream in_objectStream = new ObjectInputStream(in_byteArrayStream);
        	
				InetAddress client = in_packet.getAddress();
				in_packet.getPort();					
									

				try {
					RequestType request = (RequestType) in_objectStream.readObject();
					String pmIP = "";
					
					if(request.getType()==0){
						//this is a client heartbeat message
    
						for(int i=0;i<request.getiplist().size();i++)
						{
//							newVMList.add(request.getiplist().get(i));
							System.out.println(request.getiplist().get(i));
						}
					
//						ipmap.put(vm.pmIPaddr, true);
					}
					
					else if(request.getType()==4) // PM-VM list
					{
						ArrayList<String> oldVMList = new ArrayList<String>();
						ArrayList<String> newVMList = new ArrayList<String>();
						String pm = "";
						
						for(int i=0;i<request.getiplist().size();i++)
						{
							if(i==0)
								pm = request.getiplist().get(i);
							
							else
								newVMList.add(request.getiplist().get(i));
						}
											
						oldVMList = pmVMmap.get(pm);
						
						System.out.println("============= Updated VMs ===========");
						
						if(oldVMList != null)
						{
							for(int i=0; oldVMList.size() > i; i++)
							{
								boolean flag = false;
								
								for(int j=0; newVMList.size() > j; j++)
								{
									if(oldVMList.get(i).equals(newVMList.get(j)))
									{
										flag = true;
										break;
									}
								}
								
								if(flag == false) // this VM has been removed
								{
									System.out.println(client.getHostAddress() + "/" + pm + " :: " + oldVMList.get(i) + " Destroyed");
									String collname = client.getHostAddress() + "/" + pm + "/"+ oldVMList.get(i);
									DBCollection coll = db.getCollection(collname);
									coll.drop();
								}
							}
						}
						System.out.println("=====================================");
						pmVMmap.put(client.getHostAddress() + "/" + pm, newVMList);
					}
					
					else if(request.getType()==1){ // dependency
						String collname;
						
						ChangeInfo vm = request.getChange();
						int relation = vm.relation;
						String vmIp = vm.localhost;
						
						/**************First generate database log file***********************/
						if((vm.newclients.size()+vm.newservers.size()+vm.oldneighbors.size())>0){
							DBCollection changelogcoll = db.getCollection(Configuration.getDependencyColl());
							BasicDBObject newneighbor;
							ArrayList<BasicDBObject> newneighborlist = new ArrayList<BasicDBObject>();
							ArrayList<BasicDBObject> oldneighborlist = new ArrayList<BasicDBObject>();
							ArrayList<BasicDBObject> changeneighlist = new ArrayList<BasicDBObject>();
							Iterator<String> newit = vm.newclients.iterator();
							while(newit.hasNext()){
								String ipNeigh = newit.next();
							
								newneighbor = new BasicDBObject("ip",ipNeigh).append("type", "client").append("dependency", "request");
								newneighborlist.add(newneighbor);
							}
							newit = vm.newservers.iterator();
							while(newit.hasNext()){
								String ipNeigh = newit.next();
									
								newneighbor = new BasicDBObject("ip",ipNeigh).append("type", "server").append("dependency", this.relation_translation.get(relation));
								newneighborlist.add(newneighbor);
							}
							newit = vm.oldneighbors.iterator();
							while(newit.hasNext()){
								String ipNeigh = newit.next();
									
								newneighbor = new BasicDBObject("ip",ipNeigh).append("type", "server").append("dependency", this.relation_translation.get(relation));
								oldneighborlist.add(newneighbor);
								
								DBCollection rtLogColl = db.getCollection(Configuration.getResponsetimeColl());	
								BasicDBObject changedRT = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
									.append("VM", vmIp)
									.append("response_time", "0.0")
									.append("sourceVM", ipNeigh)
									.append("sourcePM", vm.pmIPaddr);
							
								rtLogColl.insert(changedRT);	
								
								System.out.println("removed : " + vmIp + " ------> " + ipNeigh + " (" + "0" + " ms)");							
								rtClass.updateRT(rtLogColl, ipNeigh, "0", vmIp, client.getHostAddress() + "/" + vm.pmIPaddr);
							}
							BasicDBObject changedoc = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("PM", vm.pmIPaddr)
							.append("VM", new BasicDBObject("ip", vmIp)
								.append("new", newneighborlist)
								.append("old", oldneighborlist)
	        					.append("change", changeneighlist));
								
							if(newneighborlist.size() + oldneighborlist.size() + changeneighlist.size() > 0)
								changelogcoll.insert(changedoc);
						}
						
						/**************Finishing generating log file**************************/
						//this PM must have been seen.
						collname = client.getHostAddress() + "/" + vm.pmIPaddr+"/"+vm.localhost;
						DBCollection coll = db.getCollection(collname);
						BasicDBObject query = new BasicDBObject("ip",vm.localhost);
						DBCursor cursor = coll.find(query);
						Boolean newone = true;
						if(cursor.hasNext())
							newone = false;
						
						if(newone){
							//This is the first time this vm is seen
							newone = false;
							
							ArrayList<BasicDBObject>  initialEachRT = new ArrayList<BasicDBObject>();
							ArrayList<BasicDBObject> initialUtil = new ArrayList<BasicDBObject>();
							ArrayList<BasicDBObject> initialnei = new ArrayList<BasicDBObject>();
							
//							BasicDBObject initRT = new BasicDBObject("srcVM", "0").append("srcPM", "0").append("rt", "0");
							
//							initialEachRT.add(initRT);						
							
							System.out.println("no vm before::::::::::::"+vm.localhost);
								
							BasicDBObject newvm = new BasicDBObject("id",collname)
									.append("ip", vm.localhost)
									.append("isPM", "0")
									.append("name","")
									.append("status", "alive") // initial status = "alive"
									.append("response_time", "0.0")
									.append("eachRT", initialEachRT)
									.append("util", initialUtil)
									.append("neighbor",initialnei); 

								coll.insert(newvm);
						}
						//remove disappeared neighbors
						Iterator<String> it = vm.oldneighbors.iterator();
						while(it.hasNext()){
							if(newone==false){
								String Bremoved = it.next();
								System.out.println(Bremoved+" is being removed from "+ client.getHostAddress() + "/" + vm.pmIPaddr);
								BasicDBObject removeneigh = new BasicDBObject("$pull",new BasicDBObject("neighbor",new BasicDBObject("ip",Bremoved)));
								coll.update(query, removeneigh);
							}
						}
						//add new clients
						it = vm.newclients.iterator();
						while(it.hasNext()){						
							String thisnei = it.next();
							
/*							if(rtClass.checkExistVM(thisnei) == false)
							{
								System.out.println(">> 4 NO VM > " + thisnei); 
								continue;
							}	
*/
							System.out.println(thisnei+" is being installed to "+ client.getHostAddress() + "/" + vm.pmIPaddr);
							BasicDBObject singleneigh = new BasicDBObject("ip",thisnei).append("type", "client").append("dependency","request");
							if(newone){
								newone = false;
								ArrayList<BasicDBObject> initialnei = new ArrayList<BasicDBObject>();
								System.out.println("no vm before::::::::::::"+vm.localhost);
								
								initialnei.add(singleneigh);
								BasicDBObject newvm = new BasicDBObject("ip",vm.localhost).append("neighbor",initialnei);
								coll.insert(newvm);
							}
							else{
								BasicDBObject query2 = new BasicDBObject("ip",vm.localhost).append("neighbor.ip",thisnei);
								DBCursor cursor2 = coll.find(query2);
								if(cursor2.count()>0){
									BasicDBObject newneigh = new BasicDBObject("$set",new BasicDBObject("neighbor.$",singleneigh));
									coll.update(query2, newneigh);
								}
								else{
									BasicDBObject oldneigh = new BasicDBObject("$pull",new BasicDBObject("neighbor",new BasicDBObject("ip",thisnei)));
									coll.update(query, oldneigh);
									BasicDBObject newneigh = new BasicDBObject("$push",new BasicDBObject("neighbor",singleneigh));
									coll.update(query, newneigh);
								}
							}
						}
						//add new servers
						it = vm.newservers.iterator();
						while(it.hasNext()){
					
							String thisnei = it.next();
														
							System.out.println(thisnei+" is being installed to"+client.getHostAddress() + "/" + vm.pmIPaddr);
							
							BasicDBObject singleneigh = new BasicDBObject("ip",thisnei).append("type", "server").append("dependency",this.relation_translation.get(relation));
							if(newone){
								newone = false;
								ArrayList<BasicDBObject> initialnei = new ArrayList<BasicDBObject>();
								System.out.println("no vm before::::::::::::"+vm.localhost);
								initialnei.add(singleneigh);
								BasicDBObject newvm = new BasicDBObject("ip",vm.localhost).append("neighbor",initialnei);
								coll.insert(newvm);
							}
							else{
								BasicDBObject oldneigh = new BasicDBObject("$pull",new BasicDBObject("neighbor",new BasicDBObject("ip",thisnei)));
								coll.update(query, oldneigh);
								BasicDBObject newneigh = new BasicDBObject("$push",new BasicDBObject("neighbor",singleneigh));
								coll.update(query, newneigh);
							}
						}	
					}
					else if(request.getType()==2){
						//this is a utilization info message from agent
						UtilizInfo uInfo = request.getUtil();
						pmIP = uInfo.pmIPaddr;
						//first generate changelog file
						
						DBCollection changelogcoll = db.getCollection(Configuration.getUtilizationColl());
						boolean diff[] = uInfo.diff;
						if(diff[0]==true){
							// TODO write cpu log file
							BasicDBObject changeutil = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("PM", pmIP)
							.append("cpu", Double.toString(uInfo.cpu));
							changelogcoll.insert(changeutil);
							
							if(Double.toString(uInfo.cpu) != null)
								history.updateHistory(pmIP, "CPU", Double.toString(uInfo.cpu));
						}
						if(diff[1]==true){
							// TODO write mem log file
							BasicDBObject changeutil = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("PM", pmIP)
							.append("mem", Double.toString(uInfo.mem));
							changelogcoll.insert(changeutil);
							
							if(Double.toString(uInfo.mem) != null)
								history.updateHistory(pmIP, "RAM", Double.toString(uInfo.mem));
						}
						if(diff[2]==true){
							// TODO write disk log file
							BasicDBObject changeutil = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("PM", pmIP)
							.append("disk", Double.toString(uInfo.disk));
							changelogcoll.insert(changeutil);
							
							if(Double.toString(uInfo.disk) != null)
								history.updateHistory(pmIP, "Disk", Double.toString(uInfo.disk));
						}
						if(diff[3]==true){
							// TODO write netin log file
							BasicDBObject changeutil = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("PM", pmIP)
							.append("netin", Double.toString(uInfo.netin));
							changelogcoll.insert(changeutil);

							if(Double.toString(uInfo.netin) != null)			
								history.updateHistory(pmIP, "Inbound", Double.toString(uInfo.netin));
						}
						if(diff[4]==true){
							// TODO write netout log file
							BasicDBObject changeutil = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("PM", pmIP)
							.append("netout", Double.toString(uInfo.netout));
							changelogcoll.insert(changeutil);
							
							if(Double.toString(uInfo.netout) != null)	
								history.updateHistory(pmIP, "Outbound", Double.toString(uInfo.netout));
						}						
						
						//Finish generating changelog file
						
						String collname = client.getHostAddress() + "/" + pmIP+"/"+uInfo.pmIPaddr;
						DBCollection coll = db.getCollection(collname);
						
						ArrayList<BasicDBObject>  initialEachRT = new ArrayList<BasicDBObject>();
						ArrayList<BasicDBObject> initialnei = new ArrayList<BasicDBObject>();
						//This message contains utilization information of a PM.
						if(coll.count()==0){
							//this is the first time this PM shows up
							//Prepare the new json data to be inserted into MongoDB. Notice at this time there is only utilization info
							BasicDBObject doc = new BasicDBObject("id",collname)
								.append("ip", pmIP)
								.append("isPM", "1")
								.append("name","")
				        		.append("status", "alive") // initial status = "alive"
								.append("response_time", "0.0")
								.append("eachRT", initialEachRT)
				        		.append("util", new BasicDBObject("cpu", uInfo.cpu)
				        				.append("mem", Double.toString(uInfo.mem))
				        				.append("disk", Double.toString(uInfo.disk))
				        				.append("net_in", Double.toString(uInfo.netin))
				        				.append("net_out", Double.toString(uInfo.netout)))
								.append("neighbor",initialnei); // initial delay = 0ms
							coll.insert(doc);
						}
						else{
							//this pm is already seen
							//Prepare the updated json data to be inserted into MongoDB. Notice at this time there is only utilization info

							BasicDBObject query = new BasicDBObject("ip",pmIP);
							BasicDBObject update = new BasicDBObject("$set", new BasicDBObject("status", "alive")
								.append("util.cpu", Double.toString(uInfo.cpu))
								.append("util.mem", Double.toString(uInfo.mem))
								.append("util.disk", Double.toString(uInfo.disk))
								.append("util.net_in", Double.toString(uInfo.netin))
								.append("util.net_out", Double.toString(uInfo.netout)));
							coll.update(query,update, true, false);//update the json; updated fields include: 1. alive status; 2.util info
						}
/*						collname = "PMT"+vm.pmIPaddr;
						DBCollection coll_T = db.getCollection(collname);
						if(coll_T.count()==0){
							//this is the first time this PM shows up
							//coll_T.insert();
						}
*/					}
					
					else if(request.getType()==3){ // response time
						int rtListSize = request.getDelay().size()/3; 
						
						String srcVM = "";
						String dstnVM = "";
						String rt = ""; 
						String srcPM = pmIP;

						System.out.println("\n============ response time ============");						
						for(int i=0; i< rtListSize;i++)
						{						
							srcVM = request.getDelay().get(3*i);
							dstnVM = request.getDelay().get(3*i+1);
							rt = request.getDelay().get(3*i+2); 	
							
/*							if((rtClass.checkExistVM(srcVM) == false) || (rtClass.checkExistVM(dstnVM) == false))
								continue;
*/			
							DBCollection rtLogColl = db.getCollection(Configuration.getResponsetimeColl());	
							BasicDBObject changedRT = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
							.append("VM", dstnVM)
							.append("response_time", rt)
							.append("sourceVM", srcVM)
							.append("sourcePM", srcPM);
							
							rtLogColl.insert(changedRT);						

							System.out.println(srcVM + " ------> " + dstnVM + " (" + rt + " ms)");							
							rtClass.updateRT(rtLogColl, dstnVM, rt, srcVM, srcPM);
						}
						System.out.println("=======================================\n ");			
					}
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.err.println("Class not found");
				}
			}  
		}
		catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
    
    public static void startMonitor(String script,String para){
		System.out.println("begin to exec: "+script);
        try{
            Process p = new ProcessBuilder(script, para).start();
            p.waitFor();
        }catch(IOException io){
            io.printStackTrace();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
		
	}
    
    public String num2color(double info){
    	if(info<20)
    		return "green";
    	else if(info<40)
    		return "blue";
    	else if(info<60)
    		return "yellow";
    	else if(info<80)
    		return "orange";
    	else
    		return "red";
    }
    public String num2color1000(double info){
    	if(info<200)
    		return "green";
    	else if(info<400)
    		return "blue";
    	else if(info<600)
    		return "yellow";
    	else if(info<800)
    		return "orange";
    	else
    		return "red";
    }
    
    public void printvminfo(VMInfo vm){
    	/**
    	 * This function is used only for debug purposes.
    	 * It will print out the information of the VMInfo
    	 */
    	System.out.println(vm.localhost);
    	Iterator<String> it;
    	System.out.println("All the neighbors: ");
    	for(it=vm.neighbor.iterator();it.hasNext();){
    		System.out.println(it.next());
    	}
    	System.out.println("All the clients: ");
    	for(it=vm.clients.iterator();it.hasNext();){
    		System.out.println(it.next());
    	}
    	System.out.println("All the servers: ");
    	for(it=vm.servers.iterator();it.hasNext();){
    		System.out.println(it.next());
    	}
    	System.out.println("The relation is: "+vm.relation);
    }
} 