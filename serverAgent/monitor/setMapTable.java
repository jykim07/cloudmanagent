package monitor;

import com.mongodb.*;
import java.util.*;

public class setMapTable 
{		
	static Mongo mongo;
	static DB db;
	static DBCollection mapCollection;	

	static ArrayList<DBCollection> pmCollections = new ArrayList<DBCollection>();
	static ArrayList<DBCollection> allCollections = new ArrayList<DBCollection>();
	
	static ArrayList<String> pmIDs = new ArrayList<String>();
	static ArrayList<String> vmIDs = new ArrayList<String>();
	
	static ArrayList<String> pmIPs = new ArrayList<String>();
	static ArrayList<String> vmIPs = new ArrayList<String>();
	static ArrayList<String> allIPs = new ArrayList<String>();
	
	static HashMap<String,String> mapVMIDIP = new HashMap<String,String>();
	static HashMap<String,String> mapVMIDpmIP = new HashMap<String,String>();
		
	public setMapTable(String dbIP, int dbPort, String dbName) throws MongoException  
	{	
		try {
				mongo = new Mongo(dbIP, dbPort);
				db = mongo.getDB(dbName);	
				setCollection();
		} 
		catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public setMapTable(){}
		
	public static void setCollection()
	{
		pmCollections.clear(); // used
		vmIDs.clear(); // used
		pmIPs.clear(); // used
		allIPs.clear(); // used
		
		Set<String> set = db.getCollectionNames();
		
		for(String string:set) // get names of PM/VM
		{
			DBCollection coll = db.getCollection(string);
			allCollections.add(coll);
			
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();		
			query.put("isPM",1);
			query.put("ip",1);
					
			Cursor cursor = coll.find(field,query);			
			BasicDBObject result = (BasicDBObject)cursor.next();

			String isPM = (String)result.get("isPM");
			String ip = (String)result.get("ip");	
			
			if(isPM == null)
				continue;
				
			else if(isPM.equals("1"))
			{
				pmCollections.add(db.getCollection(string));
				pmIPs.add(ip);
				allIPs.add(ip);
			}
			
			else
			{
				vmIDs.add(string);
				vmIPs.add(ip);
				allIPs.add(ip);
				mapVMIDIP.put(string, ip);
				mapVMIDpmIP.put(string, string.split("/")[0]);
			}
		}	
	}
	
	public static int getNumCollection()
	{
		int numPMs = 0;
		
		numPMs = pmCollections.size();
		
		return numPMs;
	}
	
	public static ArrayList<DBCollection> getPMCollections()
	{
		return pmCollections;	
	}
	
	public static ArrayList<DBCollection> getAllCollections()
	{
		return allCollections;	
	}
	
	public static ArrayList<String> getPMIPs()
	{
		return pmIPs;	
	}
	
	public static ArrayList<String> getAllIPs()
	{
		return allIPs;	
	}
	
	
	public static DBCollection getVMCollection(String vmID)
	{
		return db.getCollection(vmID);
	}

//======================= map ==============================//	
	public static String getVMIPfromVMID(String vmID)
	{
		return mapVMIDIP.get(vmID);
	}

	public static String getPMIPfromVMID(String vmID)
	{
		return mapVMIDpmIP.get(vmID);
	}
	
	public static String getVMIDfromVMIP(String vmIP)
	{
		Iterator<String> it = mapVMIDIP.keySet().iterator();
		String key = "";
		
		while(it.hasNext())
		{
			key = it.next();
			
			if(mapVMIDIP.get(key).equals(vmIP))
				break;	
		}
		return key; // vmID
	}
	
	public static String getPMIPfromVMIP(String vmIP)
	{
		String vmID = getVMIDfromVMIP(vmIP);
		String pmIP = getPMIPfromVMID(vmID);
		
		return pmIP; 
	}
	
	public static ArrayList<String> getMyVMs(String pmIP)
	{
		ArrayList<String> vmIPlist = new ArrayList<String>();

		for(int i=0; vmIDs.size() > i; i++)
		{		
			if(vmIDs.get(i).contains(pmIP))
				vmIPlist.add(getVMIPfromVMID(vmIDs.get(i)));
		}

		return vmIPlist;			
	}
}
