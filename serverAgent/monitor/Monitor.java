package monitor;

import configuration.*;
import java.util.HashMap;
import java.util.Map;

public class Monitor {
	static String dbIP = null;
	static int dbPort;
	
	static String dbName = null;
	static String mapCollName = null;
	static String historyCollName = null;

	public static void main(String[] args) {
		Configuration.setConfiguration();
		
		dbIP = Configuration.getDBIP();
		dbPort = Configuration.getDBPort();
		dbName = Configuration.getDBName();
		historyCollName = Configuration.getHistoryColl();
		
		Map<String,Boolean> agent_alive = new HashMap<String,Boolean>();
		Socketserver server = new Socketserver(agent_alive);
//		HeartbeatServer heartbeat = new HeartbeatServer(agent_alive);
		
		setMapTable map = new setMapTable(dbIP, dbPort, dbName);
		setHistory history = new setHistory(dbIP, dbPort, dbName, historyCollName);
		
		server.start();
//		heartbeat.start();
		history.initialize();
	}
}
