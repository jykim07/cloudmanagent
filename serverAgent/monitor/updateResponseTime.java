package monitor;

import configuration.*;
import java.util.*;
import java.lang.*;
import com.mongodb.*;
import java.text.DecimalFormat;

public class updateResponseTime
{	
	static setHistory history;
	static DB cloudmanDB;
	static ArrayList<DBCollection> collectionPM = new ArrayList<DBCollection>();
	static ArrayList<DBCollection> collectionVM = new ArrayList<DBCollection>();	
	
	static ArrayList<String> PMTcollectionNames = new ArrayList<String>();
	static HashMap<String, Double> oldPMavgRT = new HashMap<String, Double>();
	
	public updateResponseTime(DB db) throws MongoException
	{
		try {		
			cloudmanDB = db;	
		
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public static String decimalFormat(String rt)
	{
		double doubleRT = Double.parseDouble(rt);		
		DecimalFormat format = new DecimalFormat();
        format.applyLocalizedPattern("0.##");
		
		doubleRT = Double.parseDouble(format.format(doubleRT));
		rt = Double.toString(doubleRT);
		return rt;
	}
	
	public static void updateRT(DBCollection rtLogColl, String VM, String rt, String srcVM, String srcPM) 
	{		
		rt = decimalFormat(rt);		
			
		String collName = "";		
		Set<String> set = cloudmanDB.getCollectionNames();
		
		boolean flag = false;
		
		for(String string:set) // Find a db table for this VM
		{
			if(string.contains(VM))
			{
				collName = string;
				flag = true;
			}
		}
		
		if(flag == true)
		{
			DBCollection vmColl = cloudmanDB.getCollection(collName);	
			String PMIP = collName.split("/")[1];
			
			updateVMCollection(rtLogColl, vmColl, VM, PMIP, rt, srcVM, srcPM);
		}
	}
	
	public static void updateVMCollection(DBCollection rtLogColl, DBCollection vmColl, String VM, String PM, String rt, String srcVM, String srcPM) 
	{
		// update each updated response time --> VM can be observed by multiple VMs
		BasicDBObject query = new BasicDBObject("ip",VM);
		
		BasicDBObject oldRT = new BasicDBObject("$pull",new BasicDBObject("eachRT", new BasicDBObject("srcVM",srcVM)));
		vmColl.update(query, oldRT);

		BasicDBObject newRT = new BasicDBObject("$push",new BasicDBObject("eachRT", new BasicDBObject("srcVM",srcVM)
		.append("srcPM", srcPM)
		.append("rt", rt)));
			
		vmColl.update(query,newRT);	
		
		// update response time of one VM by calculating average response time
		/*		 db["PMT10.1.0.43"].aggregate({$unwind:"$eachRT"},{$match:{"ip":"10.10.10.3"}},{$project:{_id:0,"vmIP":"$ip","srcVM":"$eachRT.srcVM","rt":"$eachRT.rt"}}) */

		BasicDBObject unwind = new BasicDBObject("$unwind","$eachRT");	
		
		BasicDBObject matchQuery = new BasicDBObject("ip", VM);
		BasicDBObject match = new BasicDBObject("$match",matchQuery);
		
		BasicDBObject query2 = new BasicDBObject();		
		query2.put("vmIP","$ip");
		query2.put("srcVM","$eachRT.srcVM");
		query2.put("rt","$eachRT.rt");		
		BasicDBObject project = new BasicDBObject("$project",query2);		
	
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

		pipeline.add(unwind);		
		pipeline.add(match);
		pipeline.add(project);	
		
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
															
		Cursor cursor = vmColl.aggregate(pipeline,aggregationOptions);
		
		double rtAvg = 0;
		int cnt = 0;
		
		while(cursor.hasNext())
		{
			BasicDBObject result = (BasicDBObject)cursor.next();	
			
			String rtString = (String)result.get("rt");
			rtAvg += Double.parseDouble(rtString);
			
			cnt++;			
		}
		
		if(cnt > 0)
			rtAvg = rtAvg / cnt;
		
		// update 'response_time' of the VM with rtAvg
		BasicDBObject query3 = new BasicDBObject("ip",VM);

		BasicDBObject newAvgRT = new BasicDBObject("$set", new BasicDBObject("response_time", decimalFormat(Double.toString(rtAvg))));
		vmColl.update(query3, newAvgRT);
		
		// update history 
		history.updateHistory(VM, "RT", decimalFormat(Double.toString(rtAvg)));
		
		//update PM Collection
		updatePMCollection(rtLogColl, vmColl, PM);		
	}
	
	public static void updatePMCollection(DBCollection rtLogColl, DBCollection vmColl, String PMIP)
	{		
		// Calculate avgRT of the updated PM by querying the avgRT of VMs on the PM
//		db["PMT10.1.0.41"].aggregate({$project:{_id:0,"vmIP":"$ip","rt":"$response_time"}});
		ArrayList<String> collName = new ArrayList<String>();	
		ArrayList<DBCollection> vmCollections = new ArrayList<DBCollection>();			
		Set<String> set = cloudmanDB.getCollectionNames();
		DBCollection pmColl = null;
		
		for(String string:set) // get names of PM/VM
		{
			if(string.contains(PMIP))
				collName.add(string);
		}

		boolean found = false;

		for(int i=0; collName.size() > i; i++) 
		{			
			if(collName.get(i).contains(PMIP+"/"+PMIP)) // get PM's table
				pmColl = cloudmanDB.getCollection(collName.get(i));	
				
			else
				vmCollections.add(cloudmanDB.getCollection(collName.get(i)));
		}
		
		double rtAvg = 0;
		int cnt = 0;
		
		// update each VM's response time
		for(int i=0; vmCollections.size()>i; i++)
		{
			BasicDBObject field = new BasicDBObject();	
			BasicDBObject query = new BasicDBObject();		
			query.put("response_time",1);
					
			Cursor cursor = vmCollections.get(i).find(field,query);			
			BasicDBObject result = (BasicDBObject)cursor.next();	
				
			String rtString = (String)result.get("response_time");
				
			if(Double.parseDouble(rtString)==0)
					continue;
			
			rtAvg += Double.parseDouble(rtString);
			cnt++;							
		}

		if(cnt >0)	
			rtAvg = rtAvg / cnt;
		
		if(oldPMavgRT.get(PMIP) != null)
		{
			double oldAvgRT = oldPMavgRT.get(PMIP);
			
			if(oldAvgRT == rtAvg)
				return;
		
			// update PM collection		
			BasicDBObject query2 = new BasicDBObject("ip",PMIP);
				
			BasicDBObject newAvgRT = new BasicDBObject("$set", new BasicDBObject("response_time", decimalFormat(Double.toString(rtAvg))));
			pmColl.update(query2, newAvgRT);
				
			// update responseTimeLog collection
			BasicDBObject changedRTPM = new BasicDBObject("ts", (int)(System.currentTimeMillis()/1000))
				.append("PM", PMIP)
				.append("rt", decimalFormat(Double.toString(rtAvg)));
								
			rtLogColl.insert(changedRTPM);	
								
			// update history 
			history.updateHistory(PMIP, "RT", decimalFormat(Double.toString(rtAvg)));
				
			oldPMavgRT.put(PMIP, rtAvg);
		}
		
		else
			oldPMavgRT.put(PMIP, rtAvg);		
	}
		
	public static DBCollection getCollectionPM(int index)
	{
		return collectionPM.get(index);
	}
		
	public static String getPMIP(int index)
	{		
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query = new BasicDBObject();	
		DBCursor cursor;

		String PMIP = "";
		
		query.put("ip", 1); //Query : db.PM2.find({},{'ip':1})
		
		cursor = collectionPM.get(index).find(field,query);			
		while(cursor.hasNext())
		{	
			BasicDBObject result = (BasicDBObject)cursor.next();
			PMIP = result.getString("ip");
		}
		
		return PMIP;
	}
	
	public static int getPMIndex(String pmIP)
	{		
		BasicDBObject field = new BasicDBObject();	
		BasicDBObject query = new BasicDBObject();	
		DBCursor cursor;

		int indexPM = 0;
		
		int numCol = 0;
		int flag = 0; 
		
		while(getNumCollectionPM() > numCol)
		{
			query.put("ip", 1); //Query : db.PM2.find({},{'ip':1})
			cursor = collectionPM.get(numCol).find(field,query);	

			while(cursor.hasNext())
			{	
				BasicDBObject result = (BasicDBObject)cursor.next();
				
				if(result.getString("ip").equals(pmIP))
				{
					indexPM = numCol;
					flag = 1; // found
					break;
				}
			}
			
			if(flag==1)
				break;
				
			else
				numCol++;
		}
		
		return indexPM;
	}

	public static int getNumCollectionPM()
	{
		int numCollection = 0;
		
		numCollection = collectionPM.size();
		
		return numCollection;
	}
	
	public static int getNumCollectionPMT()
	{
		int numCollection = 0;
		
		numCollection = collectionVM.size();
		
		return numCollection;
	}
}
