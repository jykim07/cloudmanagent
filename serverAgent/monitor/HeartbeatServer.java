package monitor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class HeartbeatServer extends Thread {
	private Map<String, Boolean> ipmap;
	
	public HeartbeatServer(Map<String, Boolean>map){
		this.ipmap = map;
	}
	
	public void run(){
		while(true){
			try {
				sleep(11000);//The agent send keep-alive message every 5 seconds, so it's safe to examine every 11 second.
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			///usr/share/tomcat/webapps/ROOT/cloudman/deadPM.csv
			Iterator<String> it = ipmap.keySet().iterator();
			String deadPM = "deadPM.csv";
			BufferedWriter out;
			try {
				out = new BufferedWriter(new FileWriter(deadPM,false));
				out.write("ip");
				out.newLine();
				while(it.hasNext()){
					String agent = it.next();
					if(ipmap.get(agent)){
						//This agent is still alive
						ipmap.put(agent, false);
						System.out.println("***HEARTBEAT***"+agent+" ALIVE ");
					}
					else{
						out.write(agent);
						out.newLine();
						//ipmap.remove(agent);
						System.out.println("***HEARTBEAT***"+agent+" DEAD ");
					}
				}
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void startMonitor(String script,String para){
		System.out.println("begin to exec: "+script);
        try{
            Process p = new ProcessBuilder(script, para).start();
            p.waitFor();
        }catch(IOException io){
            io.printStackTrace();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
		
	}
}
