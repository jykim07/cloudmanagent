/*package datatype;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class VMInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	public Set<String> neighbor;
    public Set<String> clients;
    public Set<String> servers;
    public String localhost;
    public int relation = -2;
    public String owner = "n/a";
    
    public VMInfo(String localhost){
    	this.localhost = localhost;
    	this.neighbor = new HashSet<String>();
    	this.clients = new HashSet<String>();
    	this.servers = new HashSet<String>();
    }
}
*/
package datatype;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class VMInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	public Set<String> neighbor;
    public Set<String> clients;
    public Set<String> servers;
    public String localhost;
	public String pmIPaddr;
    public int relation = -2;
    public String owner = "n/a";
    
    public VMInfo(String pmIPaddr, String localhost){
		this.pmIPaddr = pmIPaddr;
    	this.localhost = localhost;
    	this.neighbor = new HashSet<String>();
    	this.clients = new HashSet<String>();
    	this.servers = new HashSet<String>();
    }
}
