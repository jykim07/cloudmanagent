package configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class Configuration {
    private static boolean debug = false;
    private static int port;
    private static int SOCKETPORT;//The port that socket servers are listening
	
    private static String dbIP = null;
    private static int dbPort;
	private static String dbName=null;
	
	private static String dependencyColl=null;
	private static String utilizationColl=null;
	private static String responsetimeColl=null;
	private static String historyColl=null;
	
	public static void setConfiguration()
	{
		try
		{
			File readFile = new File("./Config.txt");
			BufferedReader inFile = new BufferedReader(new FileReader(readFile));
			String sLine = null;
			
			while((sLine = inFile.readLine()) != null)
			{
				String parameter = sLine.split("=")[0];
				String value = sLine.split("=")[1];
				
				if(parameter.equals("debug"))
					debug = Boolean.valueOf(value);
					
				else if(parameter.equals("port"))
					port = Integer.parseInt(value);		

				else if(parameter.equals("socket port"))
					SOCKETPORT = Integer.parseInt(value);

				else if(parameter.equals("DB IP address"))
					dbIP = value;
					
				else if(parameter.equals("DB port"))
					dbPort = Integer.parseInt(value);
				
				else if(parameter.equals("DB name"))
					dbName = value;				
				
				else if(parameter.equals("dependency log"))
					dependencyColl = value;
				
				else if(parameter.equals("utilization log"))
					utilizationColl = value;
				
				else if(parameter.equals("response time log"))
					responsetimeColl = value;
										
				else if(parameter.equals("history collection"))
					historyColl = value;

				sLine = null;
			}
			inFile.close();
		}
		catch(Exception ex)
		{
	    	ex.printStackTrace();
		}
	}	

    public static boolean getDebug(){
        return debug;
    }	
	
	public static int getSocketport(){
    	return SOCKETPORT;
    }       
            
    public static int getPort(){
        return port;
    }
    
    public static String getDBIP(){
    	return dbIP;
    }
    
    public static int getDBPort(){
    	return dbPort;
    }
	
    public static String getDBName(){
    	return dbName;
    }
	
	public static String getDependencyColl(){
    	return dependencyColl;
    }

    public static String getUtilizationColl(){
    	return utilizationColl;
    }
	
	public static String getResponsetimeColl(){
    	return responsetimeColl;
    }

    public static String getHistoryColl(){
    	return historyColl;
    }
}
